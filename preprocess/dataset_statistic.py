# coding: utf-8
'''
('virus', 3339, 'drebin', 2647)
'''

def malicious_statistic(path):
    '''
    the malicious apks are how many drebin and how many virusshare
    :param path: 
    :return: 
    '''
    with open(path,"r") as f:
        virus = 0
        drebin = 0
        for i in f.readlines():
            if i.split("/")[-1].startswith("Virus"):
                # print "virus", i
                virus += 1
            else:
                drebin += 1
                # print "drebin", i
    return ("virus",virus,"drebin",drebin)

if __name__ == "__main__":
    res = malicious_statistic("malicious_apk_json_dirlist.txt")
    print res