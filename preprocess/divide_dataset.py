#just preprocess apk where there are apk files and done apk folds in same fold
import os
import shutil
import datetime
import random
'''
1.make the effective benign json to the dir of training/dataset/benign
2.make the effective malicious json to the dir of training/dataset/malicious
3.extract the malicious apk whithout manifest xml.
4.calculate the run time of preprocess.
5.give 100 random malicious apk for the result of intellidroid without any change
6.copy the manifest of benign to the dataset/benign
7.count the dedupe paths of benign of malicious 
'''
def copy_malicious_to_dataset(path):
    '''
    
    :param path:path of benign_path
    :return: null
    '''
    s = 0
    normal_apk = 0

    for i in os.listdir(path):
        apk_json = os.path.join(path,i) + "/appInfo.json"
        manifest = os.path.join(path,i) + "/AndroidManifest.xml"
        if os.path.isfile(apk_json) and (not os.path.isfile(manifest)):
            try:
                if json_info(apk_json):
                    normal_apk += 1
                    # print normal_apk
                    # dst = "/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/malicious/" + i
                    # if not os.path.exists(dst):
                    #     shutil.copytree(os.path.join(path, i),dst)
                    # print apk_json
                    # shutil.copy(apk_json, new_file_dir(apk_json))
                    with open("/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/apk_lack_manifest.txt", "a+") as f:
                        f.write(i + "\n")
                        print i

            except ValueError as e:
                print e

        s += 1
    print "all maliciois apk", s
    print "normal apk", normal_apk

def copy_benign_to_dataset(path):
    folds = ["benign_application_454_result", "benign_application_1554_result", "benign_application_2000_result",
             "benign_application_2001_result", "benign_application_2002_result", "benign_application_2003_result"]

    benign_dir = [path + "/" + i for i in folds]

    s = 0
    normal_apk = 0
    index = 0

    for dir in benign_dir:
        for i in os.listdir(dir):
            apk_dir = os.path.join(dir,i)
            # print apk_dir
            apk_json = apk_dir + "/appInfo.json"
            if os.path.isfile(apk_json):
                if json_info(apk_json):
                    # shutil.copy(apk_json,new_file_dir(apk_json)) # copy effective json to the dataset of benign


                    dir0 = "/home/chenjun/benign/"
                    dir1 = apk_json.split("/")[4].replace("result", "done")
                    dir2 = apk_json.split("/")[-2]
                    dir3 = "/apk/AndroidManifest.xml"
                    dir_manifestXML = dir0 + dir1 + "/" + dir2 + dir3
                    normal_apk += 1
                    # print "apk_json",apk_json
                    # print "manifest",dir_manifestXML

                    if os.path.isfile(dir_manifestXML):
                    #     # print True
                        index += 1
                    #     print "manifest",index
                        dst = "/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/benign/" + dir2
                        # print "dst",dst
                        # print "manifest",dir_manifestXML

                        # shutil.copy(dir_manifestXML,dst) # cop manifeset to the dataset of benign manifestxml
                    else:
                        print "err manifest",dir_manifestXML

            s += 1
    # print s
    # print normal_apk


def json_info(path):
    '''
    judge the appinfo.json effort or not.
    the columns of json file is the aacord.
    :param path: 
    :return: 
    '''
    import json
    with open(path,'r') as f:
        appinfo = json.loads(f.read())
    index  = appinfo["callPaths"]
    if index == {}:
        return False
    else:
        return True

def new_file_dir(src):
    apk_name = src.split("/")[-2]
    dst = "/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/malicious_lack_manifestXML" + "/" + apk_name
    if not os.path.exists(dst):
        os.mkdir(dst)
    return dst

def copy_malicious_apk_without_xml():
    '''
    according to the txt,search the fold and copy them to the new fold
    :return: 
    '''
    apk_without_manifest = "/home/chenjun/apk_malicious_without_xml"
    if not os.path.exists(apk_without_manifest):
        os.makedirs(apk_without_manifest)
    index = 0
    none  = 0
    with open("/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/apk_lack_manifest.txt","r") as f:
        for apk_name in f.readlines():
            # print apk_name
            virus_apk = "/home/chenjun/VirusShare_Android_20130506" + "/" + apk_name.rstrip("\n") + ".apk"
            drebin_apk = "/home/chenjun/drebin" + "/" + apk_name.rstrip("\n") + ".apk"
            if os.path.isfile(virus_apk):
                print virus_apk
                index += 1
                shutil.copy(virus_apk,apk_without_manifest)

            elif os.path.isfile(drebin_apk):
                print drebin_apk
                shutil.copy(drebin_apk,apk_without_manifest)
                index += 1

        print index
        print none

def calculate_runtime_preprocess():
    '''
    shell to preprocess of apk_malicious_without_xml
    calculate the runtime of it
    :return: 
    '''
    print os.getcwd()
    os.chdir("/home/chenjun/IntelliDroid-master/IntelliDroid-master/AppAnalysis/preprocess")
    print os.getcwd()

    start =  datetime.datetime.now()
    shell = "./PreprocessDataset.sh"
    param = "2"
    output = os.system("./shell.sh")
    endtime = datetime.datetime.now()
    runtime = (endtime - start).seconds
    print runtime


def copy_manifest_to_lack_manifest_xml():
    path = "/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/malicious_lack_manifestXML"
    manifest_path = "/home/chenjun/apk_malicious_without_xml"
    for i in os.listdir(path):
        apk_path = os.path.join(path, i)
        manifest = manifest_path + "/" + i + "/apk/AndroidManifest.xml"
        print apk_path
        print manifest
        shutil.copy(manifest,apk_path)

def copy_100randome_mali_apk():
    path = "/home/chenjun/apk_malicious_without_xml"
    dst = "/home/chenjun/random_100_maliapk/mali_apk_100"
    print len(os.listdir(path))

    for i in [random.randint(0,3520) for i in range(0,36)]:
        src = path +  "/" +  os.listdir(path)[i]
        dst = "/home/chenjun/random_100_maliapk/mali_apk_100"
        print i,src

        if os.path.exists(path):
            shutil.move(src,dst)

def copy_manifest_to_benign():
    index  = 0
    manifest_path = "/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/benign_manifestXML"
    for i in os.listdir(manifest_path):
        manifest = manifest_path + "/" + i + "/" + "AndroidManifest.xml"
        if os.path.isfile(manifest):
            index += 1
            print index,manifest
            dst = "/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/benign/" + i
            if os.path.exists(dst):
                print "dst",dst
                shutil.copy(manifest,dst)

def count_dedupe_calls(path):
    s = 0
    index = 0
    for i in os.listdir(path):

        st_after_xml = path + "/" + i + "/" + "st_after_xml.txt"
        if os.path.isfile(st_after_xml):
            # print st_after_xml

            count = len(open(st_after_xml,"r").readlines())
            print count
            index += 1

            s += count
    print index,s,s/index

############ new dataset ######################

def new_effective_virus2014():

    dir = ["/home/chenjun/VirusShare2014_6148/VirusShare2014_1_2000_result",
           "/home/chenjun/VirusShare2014_6148/VirusShare2014_2000_4000_result",
           "/home/chenjun/VirusShare2014_6148/VirusShare2014_4000_6148_result"]

    dst_dir = "/home/chenjun/VirusShare2014_6148/extra_malicious"

    manifest_dir = "/home/chenjun/VirusShare2014_6148/Manifest"

    ix = 0

    dst = "/home/chenjun/VirusShare2014_6148/VirusShare2014_4000_6148_result"
    for i in dir:
        for apk in os.listdir(i):
            appInfo = i + "/" + apk + "/" + "appInfo.json"
            if os.path.exists(appInfo):
                if json_info(appInfo):
                    ix += 1
                    print ix, appInfo
                    dst = dst_dir + "/" + apk
                    # copy effective appInfo.json to the extra/_dataset
                    # os.makedirs(dst)
                    # shutil.copy(appInfo,dst)

                    # copy manifest to dst
                    manifest = manifest_dir + "/" + apk + "/" + "AndroidManifest.xml"
                    shutil.copy(manifest, dst)


def new_effective_benign_zone():
    dir = ["/home/chenjun/benign_zone_5133/benign_zone_1_1500_result",
           "/home/chenjun/benign_zone_5133/benign_zone_1500_3000_result",
           "/home/chenjun/benign_zone_5133/benign_zone_3000_5133_result"]

    dst_dir = "/home/chenjun/benign_zone_5133/extra_benign"

    manifest_dir = "/home/chenjun/benign_zone_5133/Manifest"
    ix = 0

    for i in dir:
        for apk in os.listdir(i):
            appInfo = i + "/" + apk + "/" + "appInfo.json"
            if os.path.exists(appInfo):
                try:
                    if json_info(appInfo):
                        ix += 1
                        # print ix, appInfo
                        dst = dst_dir + "/" + apk

                        # copy effective appInfo.json to the extra/_dataset
                        # os.makedirs(dst)
                        # shutil.copy(appInfo,dst)

                        # copy manifest to dst
                        manifest =manifest_dir + "/" + apk + "/" + "AndroidManifest.xml"
                        # shutil.copy(manifest,dst)

                except Exception as err:
                    print err


if __name__ == "__main__":
    # malicious ="/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/malicious"
    # benign = "/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/benign"
    # copy_benign_to_dataset(path="/home/chenjun/benign")
    # copy_malicious_to_dataset(path="/home/chenjun/malicious/")
    # copy_malicious_apk_without_xml()
    # calculate_runtime_preprocess()
    # copy_manifest_to_lack_manifest_xml()
    # copy_100randome_mali_apk()
    print 1
    # # copy_manifest_to_benign()
    # count_dedupe_calls(malicious)
    # print float(68  / 105)
    new_effective_virus2014()
    # new_effective_benign_zone()