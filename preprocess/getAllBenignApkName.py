# coding: utf-8

import os

def getBenignApkName():
    path =  "/home/chenjun/benign"
    folds = ["benign_application_454_result", "benign_application_1554_result", "benign_application_2000_result",
             "benign_application_2001_result", "benign_application_2002_result", "benign_application_2003_result"]

    benign_dir = [path + "/" + i for i in folds]
    with open("Analyzed_BenignApk_name", "w+") as f:
        for i in benign_dir:
            print i
            for apk in os.listdir(i):
                print apk
                print type(apk)
                f.write(apk + '\n')


if __name__ == "__main__":
    getBenignApkName()