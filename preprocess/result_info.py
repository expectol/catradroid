# coding: utf-8
import os
"""
[('malicious_number', 10308), ('null_apk', 981), ('only_manifest', 265), ('only_json', 6427), ('normal_apk', 2634)]
[('benign_number', 10015), ('null_apk', 4381), ('only_info', 3705), ('normal_apk', 1929)]

2019.1.25, upgrade
[('benign_number', 10015), ('null_apk', 4384), ('only_info', 3702), ('only_info_effort', 37), ('normal_apk', 1929)]
so, the number of effective benign_1966 result is 1929+37=1966.
[('malicious_number', 10307), ('null_apk', 981), ('only_manifest', 265), ('only_json', 6427), ('only_json_effort', 3352), ('normal_apk', 2634)]
so, the number of effective malicious result is 3352+2634=5986.
"""

def maliciou_info(path):
    # count the non-apk file in malicious result folds
    # judge whether the fold contains AndrodManifest.xml
    # output the effort file to the effort.txt

    sum = 0
    null_apk = 0
    only_manifest = 0
    only_json = 0
    only_json_effort = 0
    normal_apk = 0

    only_json_useless = 0
    for apk in os.listdir(path):
        sum += 1
        apk_dir = os.path.join(path + '/', apk)
        if len(os.listdir(apk_dir)) == 0:
            null_apk += 1
        if len(os.listdir(apk_dir)) == 1:
            if os.listdir(apk_dir)[0] == "AndroidManifest.xml":
                only_manifest += 1
                # print os.listdir(apk_dir),apk
            elif os.listdir(apk_dir)[0] == "appInfo.json":
                only_json += 1
                res = json_info(apk_dir + "/" + os.listdir(apk_dir)[0])
                if res == True:
                    only_json_effort += 1
                    #save_to_txt(path="malicious_apk_json_dirlist.txt", apkdir=apk_dir)
                elif res == False:
                    only_json_useless += 1
                # print os.listdir(apk_dir), apk
        if len(os.listdir(apk_dir)) >= 2:
            normal_apk += 1
            #save_to_txt(path="malicious_apk_json_dirlist.txt", apkdir=apk_dir)

    return [("malicious_number",sum), ("null_apk",null_apk), ("only_manifest",only_manifest),
            ("only_json",only_json),("only_json_effort",only_json_effort),("normal_apk",normal_apk)]


def fold_info(path):
    '''
    for the benign_1966 result ,record the benign_1966 info:
    1.the number of efford
    2.the text of efford name
    :param path: 
    :return: 
    '''
    i = 0

    null_apk = 0
    only_appinfo_apk = 0
    only_appinfo_effort = 0
    normal_apk = 0


    for apk in os.listdir(path):
        apk_dir = os.path.join(path + '/', apk)
        if len(os.listdir(apk_dir)) == 0:
            null_apk += 1
        if len(os.listdir(apk_dir)) == 1:
            json_path = apk_dir + "/" + os.listdir(apk_dir)[0]
            #print json_path
            res = json_info(json_path)
            #print res
            if res == True:
                only_appinfo_effort += 1
                # print json_path
            only_appinfo_apk += 1
        if len(os.listdir(apk_dir)) > 1:
            normal_apk += 1
        i += 1
    return i, null_apk,only_appinfo_apk,only_appinfo_effort,normal_apk


def benign_info(path):
    folds = ["benign_application454_result", "benign_application_1554_result", "benign_application_2000_result",
             "benign_application_2001_result", "benign_application_2002_result", "benign_application_2003_result"]

    benign_dir = [path + "/" + i for i in folds]

    sum = 0
    null_apk = 0
    only_appinfo_apk = 0
    only_appinfo_effort = 0
    normal_apk = 0

    for i in benign_dir:
        res = fold_info(i)
        sum += res[0]
        null_apk += res[1]
        only_appinfo_apk += res[2]
        only_appinfo_effort += res[3]
        normal_apk += res[4]

    return [("benign_number",sum), ("null_apk",null_apk), ("only_info",only_appinfo_apk),
            ("only_info_effort",only_appinfo_effort),("normal_apk",normal_apk)]
# [('benign_number', 10015), ('null_apk', 4381), ('only_info', 3705), ('normal_apk', 1929)]


def json_info(path):
    '''
    judge the appinfo.json effort or not.
    the columns of json file is the aacord.
    :param path: 
    :return: 
    '''
    import json
    with open(path,'r') as f:
        appinfo = json.loads(f.read())
    index  = appinfo["callPaths"]
    if index == {}:
        return False
    else:
        return True



def save_to_txt(path,apkdir):
    with open(path, "a+") as f:
        f.write(apkdir + "\n")


if __name__ == "__main__":
    r = maliciou_info(path = "/home/chenjun/malicious")
    print r
    #r = benign_info(path= "/home/chenjun/benign_1966")
    #print r
    #a = "/home/chenjun/malicious/0a0a78000e418ea28fa02e8c162c43396db6141ef8fe876db4027fef04bed663/appInfo.json"
    #b = "/home/chenjun/benign_1966/benign_application454_result/000A9DE114530246823FAC953D703AAC17875DB35B36741FC847198598F9A363/appInfo.json"

    #r = json_info(a)
    #print r
