#!/usr/bin/env python
# -*- coding: utf-8 -*-
#! /usr/bin/python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold, StratifiedKFold

from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB

from sklearn import metrics
from sklearn.metrics import confusion_matrix,roc_curve,auc
import matplotlib.pyplot as plt


def androidDataset():
    data = pd.read_csv('D:\机器学习实验代码\demo\\result.csv', engine='python')
    target = "Category"
    IDcol = "ID"
    data_fold = []
    x_columns = [x for x in data.columns if x not in [target, IDcol]]
    X = data[x_columns]
    y = data[target].replace(to_replace={"malicous", "benign"}, value={1, 0})
    # # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=0)

    # 10-fold
    kf = KFold(n_splits=2)
    kf.get_n_splits(X)
    for train_index, test_index in kf.split(X):
        X_train = X.iloc[train_index]
        X_test = X.iloc[test_index]
        y_train, y_test = y.iloc[train_index], y.iloc[test_index]
        data_fold.append([X_train, X_test, y_train, y_test])
    return data_fold

    # return X_train, X_test, y_train, y_test

def irisDataset():
    iris = datasets.load_iris()
    iris_x = iris.data[iris.target != 2] # 150 * 4:150 samples,4 features
    iris_y = iris.target[iris.target != 2] # 150 * 1: 1, class

    # Add noisy features to make the problem harder
    random_state = np.random.RandomState(0)
    n_samples, n_features = iris_x.shape
    iris_x = np.c_[iris_x, random_state.randn(n_samples, 200 * n_features)]
    X_train,X_test,y_train,y_test = train_test_split(iris_x, iris_y, test_size=0.3, random_state=0)

    # 10 foldsww

    return X_train, X_test, y_train, y_test


def randomForest(dataset):

    X_train, X_test, y_train, y_test = dataset

    randomForest = RandomForestClassifier()
    randomForest.fit(X_train, y_train)

    y_predict = randomForest.predict(X_test)
    acc = randomForest.score(X_test, y_test)
    pr = metrics.precision_score(y_true=y_test, y_pred=y_predict, average="macro")
    rc = metrics.recall_score(y_true=y_test, y_pred=y_predict, average="macro")
    M = confusion_matrix(y_true=y_test, y_pred=y_predict)
    print("ACC", acc)
    print("Precison", pr)
    print("Recall", rc)
    print("confusion Matrix: \n", M)

    # 首先对n_estimators进行网格搜索确定最佳分类效果
    y_score = (randomForest.predict_proba(X_test))[:, 1]
    fpr, tpr, threshold = roc_curve(y_test, y_score)
    roc_auc = auc(fpr, tpr)
    print("AUC:", roc_auc )
    lw = 2
    plt.figure(figsize=(10, 10))
    plt.plot(fpr, tpr, color='darkorange',
             lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('RandomForest Algorithm')
    plt.legend(loc="lower right")
    # plt.show()
    return acc, pr, rc, roc_auc

def knn(dataset):
    X_train, X_test, y_train, y_test = dataset
    knn = KNeighborsClassifier(n_neighbors=5, algorithm='ball_tree')
    knn.fit(X_train,y_train)
    y_predict = knn.predict(X_test)  # according to the data and give the  prediction
    probility = (knn.predict_proba(X_test))[:,1]  # give the prob of echo class
    acc = knn.score(X_test, y_test)
    pr = metrics.precision_score(y_true=y_test,y_pred=y_predict, average="macro")
    rc = metrics.recall_score(y_true=y_test,y_pred=y_predict, average="macro")
    M = confusion_matrix(y_true=y_test, y_pred=y_predict)
    print("ACC", acc)
    print("Precison", pr)
    print("Recall", rc)
    print("confusion Matrix: \n", M)

    fpr, tpr, threshold = roc_curve(y_test, probility)
    roc_auc = auc(fpr,tpr)
    lw = 2
    plt.figure(figsize=(10, 10))
    plt.plot(fpr, tpr, color='darkorange',
             lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('k-NN Algorithm')
    plt.legend(loc="lower right")
    # plt.show()
    print("AUC:", roc_auc )

    return acc, pr, rc,roc_auc

def svm(dataset):
    X_train, X_test, y_train, y_test = dataset

    svm = SVC(kernel='linear')
    svm.fit(X_train, y_train)

    y_predict = svm.predict(X_test)

    acc = svm.score(X_test, y_test)
    pr = metrics.precision_score(y_true=y_test, y_pred=y_predict, average="macro")
    rc = metrics.recall_score(y_true=y_test, y_pred=y_predict, average="macro")
    M = confusion_matrix(y_true=y_test, y_pred=y_predict)
    print("ACC", acc)
    print("Precison", pr)
    print("Recall", rc)
    print("confusion Matrix: \n", M)

    y_score = svm.decision_function(X_test)
    fpr, tpr, threshold = roc_curve(y_test, y_score)
    roc_auc = auc(fpr, tpr)
    lw = 2
    plt.figure(figsize=(10, 10))
    plt.plot(fpr, tpr, color='darkorange',
             lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('SVM Algorithm')
    plt.legend(loc="lower right")
    # plt.show()
    print("AUC:", roc_auc )

    return acc, pr, rc, roc_auc


def naiveBayes(dataset):
    '''
    高斯朴素贝叶斯,多项式分布,伯努利朴素贝叶斯
    :return:
    '''
    X_train, X_test, y_train, y_test = dataset
    bayes = GaussianNB()
    bayes.fit(X_train, y_train)

    y_predict = bayes.predict(X_test)

    acc = bayes.score(X_test, y_test)
    pr = metrics.precision_score(y_true=y_test, y_pred=y_predict, average="macro")
    rc = metrics.recall_score(y_true=y_test, y_pred=y_predict, average="macro")
    M = confusion_matrix(y_true=y_test, y_pred=y_predict)
    print("ACC", acc)
    print("Precison", pr)
    print("Recall", rc)
    print("confusion Matrix: \n", M)

    y_score = (bayes.predict_proba(X_test))[:, 1]
    fpr, tpr, threshold = roc_curve(y_test, y_score)
    roc_auc = auc(fpr, tpr)
    lw = 2
    plt.figure(figsize=(10, 10))
    plt.plot(fpr, tpr, color='darkorange',
             lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('NaiveBayes Algorithm')
    plt.legend(loc="lower right")
    # plt.show()
    print("AUC:", roc_auc )

    return acc, pr, rc, roc_auc

if __name__ == "__main__":
    # dataset = irisDataset()
    acc_average, pr_average, rc_average, auc_average = 0, 0, 0, 0
    data = pd.read_csv(r'D:\expectol-androidsecexperiment-d50c3ffba3d7\api_miner\experiments\result_api_miner.csv', engine='python')
    # data = pd.read_csv(r'D:\机器学习k实验代码\demo\result.csv', engine='python')
    target = "Category"
    IDcol = "ID"
    x_columns = [x for x in data.columns if x not in [target, IDcol]]
    X = data[x_columns]
    # print(data[target])
    y = data[target].replace(to_replace={"malicious", "benign"}, value={1, 0})
    # print(y)
    # ROC,AUC
    # 处理正类负类不均衡的样本
    all ,ix, index = 0, 0, 0
    for i in y:
        if i == 1:
            ix += 1
        else:
            index += 1
        all += 1
    # print(all ,ix,index)
    extra_benign = data.loc[data[target] == "benign"].sample(n=3, replace=True, axis=0)
    # print(extra_benign)
    new_data = data.append(extra_benign)
    x_columns = [x for x in new_data.columns if x not in [target, IDcol]]
    X = new_data[x_columns]
    y = new_data[target].replace(to_replace={"malicious", "benign"}, value={1, 0})

    # 选取百分之30进行交叉验证
    # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1)
    # svm((X_train, X_test, y_train, y_test))
    # knn((X_train, X_test, y_train, y_test))
    # randomForest((X_train, X_test, y_train, y_test))
    # naiveBayes((X_train, X_test, y_train, y_test))

    # 10-fold
    skf = StratifiedKFold(n_splits=5)
    skf.get_n_splits(X, y)
    for train_index, test_index in skf.split(X,y):
        X_train, X_test = X.iloc[train_index], X.iloc[test_index]
        y_train, y_test = y.iloc[train_index], y.iloc[test_index]
        acc, pr, rc, AUC = svm((X_train, X_test, y_train, y_test))
        acc_average += acc
        pr_average += pr
        rc_average += rc
        auc_average += AUC
    print("acc:{},pr:{},rc:{},auc:{}".format(acc_average/5, pr_average/5, rc_average/5, auc_average/5))
    # print("随机森林：")
    # randomForest((X_train, X_test, y_train, y_test))
    # print("k近邻算法:")
    # knn(dataset)
    # print("支持向量机算法:")
    # svm(dataset)
    # print("朴素贝叶斯算法:")
    # naiveBayes(dataset)