# -*- coding: utf-8 -*-

import os
import os, sys
import json
import types
import os.path
from parseManifest import *

def getJsonDir(path):
    a = []
    i = 0
    for file in os.listdir(path):
        file_path = os.path.join(path,file)
        # for file in os.listdir(file_path):
        #     file_path_path = os.path.join(file_path,file)
        #     if os.path.split(file_path_path)[1] == 'appInfo.json':
        #         a.append(file_path_path)
        #         i = i + 1
        #         #print a
        json = file_path + "/appInfo.json"
        a.append(json)
        # print i,json
        i += 1

    return a


def start_target(data):

    def matchkey(dict, objkey1, objkey2):
        tmp = dict
        for k, v in tmp.items():

            if k == objkey1:
                # global i
                # a.append(tmp.get(k))
                # print str(v)
                startMethod.append(str(tmp.get(k).encode('utf-8')) + " -> ")
                # i += 1

            if k == objkey2:
                # print str(v)
                targetMethod.append(str(tmp.get(k).encode('utf-8')))

            else:
                if type(v) is types.DictType:
                    matchkey(v, objkey1, objkey2)

    startMethod = []
    targetMethod = []

    matchkey(data, "startMethod", "targetMethod")
    result = map(lambda (a, b): a + b, zip(startMethod, targetMethod))
    return result


def writeToTxt(list_name, file_path):
    try:
        fp = open(file_path, "a+")
        for item in list_name:
            fp.write(str(item) + "\n")
        fp.close()
    except IOError:
        print("fail to open file")

def build_start_target(path):
    '''
    build the init start_target without process to Manifest.xml
    :param path: 
    :return: 
    '''
    i = 0
    # print 1
    all_path = 0
    for jsondir in getJsonDir(path):
        i += 1
        print i, jsondir
        f = open(jsondir, 'r')
        data = json.load(f, encoding='utf-8')
        res = start_target(data)
        print len(res)
        all_path += int(len(res))
        filepath = os.path.dirname(jsondir) + "/start_target.txt"
        print filepath
        writeToTxt(res, filepath)
    print all_path

def build_start_target_after_xml(path):
    index = 0
    err = 0
    for i in os.listdir(path):
        manifest = path + "/" + i +"/" + "AndroidManifest.xml"
        st_txt = path + "/" + i +"/" + "start_target.txt"
        if os.path.exists(st_txt):

            with open(manifest, 'r') as f:
                try:
                    doc = parse(f)
                    index = index + 1
                    # print str(index) + ":", doc

                    with open(st_txt, 'r') as STfile:
                        stlist = STfile.read()

                        # replace activity
                        for activity in getSourceActivityList(doc):
                            if activity in stlist:
                                stlist = stlist.replace(activity, "android.app.Activity")
                        # replace service
                        for service in getSourceServiceList(doc):
                            if service in stlist:
                                stlist = stlist.replace(service, "android.app.Service")

                        # replace receiver
                        for reiceiver in getSourceReceiverList(doc):
                            if reiceiver in stlist:
                                stlist = stlist.replace(reiceiver, "android.app.receiver")
                        # replace provider
                        for provider in getSourceProviderList(doc):
                            if provider in stlist:
                                stlist = stlist.replace(provider, "android.app.provider")
                        # 将packagename删掉
                        if getPackagename(doc) in stlist:
                            stlist = stlist.replace(getPackagename(doc), "")

                        # 将更改后的STlist输入到新的文件中
                        f = open(os.path.dirname(st_txt) + '/st_after_xml.txt', 'w+')
                        f.write(stlist)
                        # # 保存不重复的结果
                        f = open(os.path.dirname(st_txt) + '/st_after_xml.txt', 'r')
                        alltext = (line.lstrip(".") for line in f.readlines())
                        # f.close()
                        # 将去除重复的结果，生成standardSTlist
                        f = open(os.path.dirname(st_txt) + '/st_after_xml.txt', 'w')
                        for line in list(dedupe(alltext)):
                            f.write(line)
                        f.close()
                except Exception, e:
                    err = err + 1
                    print "这不是一个良构的XML文档"
                    print "可能原因:", e
                    print "错误xml个数:" + str(err) + manifest

def feature_lib(path):
    feature_lib_name = os.path.basename(path) + "_feature_lib.txt"
    if os.path.isfile(feature_lib_name):
        os.remove(feature_lib_name)
    line_number = 0
    all_txt = []
    with open(feature_lib_name, "a+") as lib_file:
        for i in os.listdir(path):
            st_after_xml = path + "/" + i + "/" + "st_after_xml.txt"
            if os.path.isfile(st_after_xml):
                with open(st_after_xml,"r") as f:
                    txtlines = f.readlines()
                    for line in txtlines:
                        print line
                        line_number += 1
                        all_txt.append(line)
        n = 0
        for line in list(dedupe(all_txt)):
            n += 1
            lib_file.write(line)

    print line_number,n

def creat_feature_lib():
    benign_lib = "extra_benign_feature_lib.txt"
    malicious_lib = "extra_malicious_feature_lib.txt"
    feature = "extra_feature_lib.txt"
    res = []
    i,j,index = 0,0,0
    ads = 0
    with open(benign_lib,"r") as f1:
        for line in f1.readlines():
            i += 1
            res.append(line)
    with open(malicious_lib,"r") as f2:
        for line in f2.readlines():
            j += 1
            res.append(line)
    with open(feature,"w") as f3:
        for line in list(dedupe(res)):
            index += 1
            if "ads" in line:
                ads += 1
            f3.write(line)

    print i,j,len(res),index
    print "include ads",ads

def merge_feature_libs():

    extrafeaturelib = 'benign_feature_lib.txt'
    featurelib = 'extra_benign_feature_lib.txt'

    all_feature_lib = 'merge_benign_feature_lib.txt'

    res = []
    i, j, index = 0,0,0
    ads = 0
    with open(featurelib,'r') as f1:
        for line in f1.readlines():
            i +=1
            res.append(line)
    with open(extrafeaturelib, 'r') as f2:
        for line in f2.readlines():
            j += 1
            res.append(line)
    with open(all_feature_lib, 'w') as f3:
        for line in list(dedupe(res)):
            index += 1
            if 'ads' in line:
                ads += 1
            f3.write(line)
    print i,j,len(res),index
    print "include ads", ads



if __name__ == '__main__':
    # malicious = "/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/malicious"
    # benign = "/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/benign"
    # build_start_target(path)
    # print 1
    # build_start_target_after_xml(malicious)
    # feature_lib(malicious)
    # creat_feature_lib()
    extra_malicious = '/home/chenjun/VirusShare2014_6148/extra_malicious'
    extra_benign = '/home/chenjun/benign_zone_5133/extra_benign'
    # build_start_target(extra_malicious)
    # build_start_target_after_xml(extra_malicious)
    # build_start_target(extra_benign)
    # build_start_target_after_xml(extra_benign)
    # feature_lib(extra_benign)

    # creat_feature_lib()
    merge_feature_libs()