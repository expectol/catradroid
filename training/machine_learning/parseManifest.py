#! /usr/bin/python
# -*- coding: utf-8 -*-
try:

    import xml.etree.cElementTree as ET

except ImportError:

    import xml.etree.ElementTree as ET
from xml.etree.ElementTree import parse
import os.path
import os
def getManifest(path):
    a = []
    for file in os.listdir(path):
        apkfile = os.path.join(path,file)
        for file in os.listdir(apkfile):
            if file == 'AndroidManifest.xml':
                manifest = os.path.join(apkfile,file)
                a.append(manifest)
                # print a
    return a

#找到每个apk的packagename
def getPackagename(doc):
   manfest_tag = doc.getroot()
   return manfest_tag.attrib['package']

# 找到所有的acitivit
def getSourceActivityList(doc):
    nodelist = doc.findall('application/activity')
    act_list = []
    for node in nodelist:
        act_list.append(node.get('{http://schemas.android.com/apk/res/android}name'))
    return act_list

#找到所有的service
def getSourceServiceList(doc):
    nodelist = doc.findall('application/service')
    ser_list = []
    for node in nodelist:
        ser_list.append(node.get('{http://schemas.android.com/apk/res/android}name'))
    return ser_list

#找到所有的receiver
def getSourceReceiverList(doc):
    nodelist = doc.findall('application/receiver')
    rec_list = []
    for node in nodelist:
        rec_list.append(node.get('{http://schemas.android.com/apk/res/android}name'))
    return rec_list

#找到所有的provider
def getSourceProviderList(doc):
    nodelist = doc.findall('application/provider')
    pro_list = []
    for node in nodelist:
        pro_list.append(node.get('{http://schemas.android.com/apk/res/android}name'))
    return pro_list
#去除列表中重复的行
def dedupe(items):
    seen = set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)


# 读取每个apk的STlist文件讲其中的四大组件替换成标准名字
# 替换规则:
# activity : android.app.Activity
# service  : android.app.Service
# reiceiver: android.app.Reiceiver
# provider : android.app.Provider
if __name__ == '__main__':
    i = 0
    err = 0
    for file in getManifest('/home/chenjun/demo/drebin1000'):
        print file
        apkfile = os.path.dirname(file)
        # print apkfile
        txtfile = apkfile + "/STlist.txt"
        print txtfile


        if os.path.exists(txtfile):


             with open(file, 'r+') as f:
                 try:
                    doc = parse(f)
                    i = i + 1
                    print "第" + str(i) + "个：", doc
                    f.close()



                    with open(txtfile,'r+') as STfile:
                        stlist = STfile.read()
                        STfile.close()

                        #replace activity
                        for activity in getSourceActivityList(doc):
                            if activity in stlist:
                                stlist = stlist.replace(activity,"android.app.Activity")
                        #replace service
                        for service in getSourceServiceList(doc):
                            if service in stlist:
                                stlist = stlist.replace(service,"android.app.Service")

                        #replace receiver
                        for reiceiver in getSourceReceiverList(doc):
                            if reiceiver in stlist:
                                stlist = stlist.replace(reiceiver,"android.app.receiver")
                        #replace provider
                        for provider in getSourceProviderList(doc):
                            if provider in stlist:
                                stlist = stlist.replace(provider,"android.app.provider")
                        #将packagename删掉
                        if getPackagename(doc) in stlist:
                            stlist = stlist.replace(getPackagename(doc),"")

                        # 将更改后的STlist输入到新的文件中
                        f = open(os.path.dirname(txtfile) + '/newSTlist.txt','w+')
                        f.write(stlist)
                        f.close
                        #保存不重复的结果
                        f = open(os.path.dirname(txtfile) + '/newSTlist.txt', 'r+')
                        alltext = (line.lstrip(".") for line in f.readlines())
                        f.close()
                        #将去除重复的结果，生成standardSTlist
                        f = open(os.path.dirname(txtfile) + '/standardSTlist.txt', 'w+')
                        for line in list(dedupe(alltext)):
                            f.write(line)
                        f.close()
                 except Exception, e:
                     err = err + 1
                     print "这不是一个良构的XML文档"
                     print "可能原因:",e
                     print "错误xml个数:"  + str(err)