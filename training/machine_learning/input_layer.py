#! /usr/bin/python
# -*- coding: utf-8 -*-
import json
import os
import os.path
import types
from io import open
#将文件和标准文件比对，在文件的
def one_hot(featureLib, st_after_xml):

    vector = []
    with open(featureLib,'r') as f1:
        with open(st_after_xml,'r') as f2:
            feature = f1.read().splitlines()
            sample = f2.read().splitlines()
            for i in feature:
                if sample.__contains__(i):
                    vector.append(1)
                    # print "have calls"
                else:
                    vector.append(0)

    return vector

def build_vector(path):
    index = 0
    featureLib = "feature_lib.txt"

    for i in os.listdir(path):
        st_after_xml = os.path.join(path,i) + "/" + "st_after_xml.txt"
        if os.path.exists(st_after_xml):
            print st_after_xml
            vector = one_hot(featureLib,st_after_xml)
            index += 1
            vectorfile = os.path.join(path,i) +  '/' + 'vector.txt'
            print index,vectorfile
            with open(vectorfile, "w") as f:
                for i in vector:
                    f.write(unicode(i))

def generateCSV():

    path = ["/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/malicious","/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/benign"]
    # for i in path:
    #     for apk in os.listdir(i):
    #         vector_txt = os.path.join(i,apk) + "/" + "vector.txt"

    vector_file = [os.path.join(i,apk) + "/" + "vector.txt"for i in path for apk in os.listdir(i) if os.path.exists(os.path.join(i,apk) + "/" + "vector.txt") ]
    # print vector_file
    # print len(vector_file)
    index = 0
    with open("dataset.csv", "w") as csv:
        first_line = "ID"+ ",F" * len(vector_file) + "," + "Category"
        # first_line = first_line.encode('utf-8')
        first_line = first_line.decode('utf-8')
        csv.write(first_line + '\n')
        for i in vector_file:
            class_name = i.split("/")[-3]
            apkname = i.split("/")[-2]
            index += 1
            print index
            with open(i,'r') as f:
                vector = f.read()
                vector_csv = apkname + "," +",".join(vector) + "," + class_name
                print vector_csv
                csv.write(vector_csv + "\n")




if __name__ == "__main__":
    malicious = "/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/malicious"
    benign = "/home/chenjun/wait_to_commit/androidsecexperiment/training/dataset/benign"
    # build_vector(benign)
    # generateCSV()
    with open("dataset.csv",'r') as f:
        for i in f.readlines():
            print i
