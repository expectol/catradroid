#! /usr/bin/python
# -*- coding: utf-8 -*-

import os
from collections import OrderedDict


def calculate_callpath_weights():
    dir = ["/home/chenjun/Documents/repos/catradroid/training/dataset/benign",
           "/home/chenjun/Documents/repos/catradroid/training/dataset/extra_benign",
           "/home/chenjun/Documents/repos/catradroid/training/dataset/malicious",
           "/home/chenjun/Documents/repos/catradroid/training/dataset/extra_malicious"]
    cnt = 0
    ix = 0
    call_path_weight = {} # "call path": "weight"
    for i in dir:
        for j in  os.listdir(i):
            cnt += 1
            call_path = os.path.join(i,j) + "/" + "st_after_xml.txt"
            # print cnt, call_path
            if os.path.isfile(call_path):
                ix += 1
                with open(call_path,'r') as f:
                    line = f.readline().rstrip("\n")
                    while line:
                        call_path_weight[line] = call_path_weight.get(line,0) + 1
                        line = f.readline().rstrip("\n")

    # for k in call_path_weight.keys():
    #     print k,call_path_weight[k]
    # print call_path_weight
    # print len(call_path_weight)
    weight = OrderedDict(sorted(call_path_weight.items(),key=lambda x:x[1],reverse=True))
    # print weight
    # for i in weight.keys():
    #     print i,weight[i]
    return weight

if __name__ == "__main__":
    weight = calculate_callpath_weights()
    with open("path_weight.txt", "w") as f:
        for i in weight.keys():
            line = i + "******" + str(weight[i])
            print line
            f.write(line + "\n")
