# CatraDroid
CatraDroid实现基于敏感调用路径的Android恶意软件检测方法，其提取敏感调用路径基于[IntelliDroid](https://github.com/miwong/IntelliDroid)静态分析部分的改进，其数据处理和机器学习训练的数据和代码均保存在本仓库中。本仓库的所有配置环境已保存在requirements.txt文件中，部署前，需要执行如下代码：

```
pip install -r requirements.txt
```
相关依赖包即可自动安装。

---

### NLP : 自然语言处理来获取Android敏感api

- 获取恶意语句知识库：运行extract_malicious_sent.py来从https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=android上爬取所有的描述android上的恶意行为的语句组成知识库，保存为malicious_behavior_lib.txt
- 从知识库中提取敏感关键词：运行calc_sensitive_keys.py来计算malicious_behavior_lib.txt知识库中每个词的TF-IDF值，并按值排序，结果保存到sensitive_key.txt。
- 从Android官方网站上爬取所有的android API：运行extract_android_api.py，将爬取的所有android API保存到Android_API.txt
- 用前400个敏感关键词从Android官方api中匹配出潜在的敏感api：运行match_sensitive_API.py，匹配结果保存在pre_sensitive_api.txt中
- 将获取的潜在敏感api格式转换成smali语法格式：运行java_to_smali.py，结果保存在sensitive_smali_api.txt中(其中的java_classes.txt与Android_classes.txt文件是用于转换的）
- 手动从匹配出的潜在敏感api中标记出641个真实的敏感api，作为最终确定的android敏感api，保存在sensitive_api.txt中

---

### Appanalysis : 静态分析提取敏感调用路径

首先，根据IntelliDroid的README.md文件保证IntelliDroid可以正常运行，其主要文件，如下：

- preprocess： 使用Dare和Apktool进行反编译

- targetedMethods.txt: 敏感目标API列表

- process***.sh: 对大量apk提取敏感调用路径的shell脚本

---

### preprocess ： 对提取的敏感调用路径进行特征预处理

主要代码文件及其完成功能，如下：

- dataset_statistic.py : 统计执行的良性样本集合和恶意样本集合来自何种软件家族

- divide_dataset.py : 1.make the effective benign json to the dir of training/dataset/benign
    2.make the effective malicious json to the dir of training/dataset/malicious
    3.extract the malicious apk whithout manifest xml.
    4.calculate the run time of preprocess.
    5.give 100 random malicious apk for the result of intellidroid without any change
    6.copy the manifest of benign to the dataset/benign
    7.count the dedupe paths of benign of malicious 

- result_info.py : 统计样本信息，判断生成的json文件是否合法

---

### training : 机器学习训练的实现部分

本部分包括训练数据集，learning，和路径权重的实现部分。

- dataset: 用于训练的有效样本，extra_malicious,extra_benign,额外增加的有效实验结果

- machine_learning: 包括特征提取extract.py,处理成特征向量input_layer.py,分类器训练train_layer.py

- weights: 包括计算路径权重的代码call_path_weights.py, 记录路径的权重文件path_weight.txt



