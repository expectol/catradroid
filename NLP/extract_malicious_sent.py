#从CVE网页上爬取描述Android恶意行为的句子

from bs4 import BeautifulSoup
import requests

url = 'https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=android'

wb_data = requests.get(url)
soup = BeautifulSoup(wb_data.text, 'lxml')
with open("malicious_behavior_lib.txt","a+") as malicious_behavior_lib_file:
    for malicious_behavior in soup.find_all('td',valign="top"):
        malicious_behavior_string = malicious_behavior.get_text().strip()
        if malicious_behavior_string.find("CVE-")==-1:
            malicious_behavior_lib_file.write(malicious_behavior_string+"\n")
