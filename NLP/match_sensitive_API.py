#用敏感关键词匹配Android官方API得到敏感路径，K值表示参数匹配的前K个敏感关键词
import nltk
#读取前K个敏感关键词
def get_sensitive_key_word(K):
    key_words=[]
    with open("sensitive_key.txt","r",encoding='UTF-8') as sensitive_key_file:
        for line in sensitive_key_file.readlines():
            if len(key_words)>=K:
                break
            line=line.strip("\n")
            key_words.append(line)
    return key_words

#读取所有的Android api
def get_Android_api():
    Android_apis={}
    Android_apis_string="&"
    with open("Android_API.txt","r",encoding='UTF-8') as Android_api_file:
        for line in Android_api_file.readlines():
            line = line.strip("\n")
            low_api = line.lower()
            tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')
            low_api_list = tokenizer.tokenize(low_api)
            Android_apis[line]=low_api_list
    return Android_apis

if __name__ == "__main__":
    K=300
    ori_key_words=get_sensitive_key_word(K)
    key_words=[]
    #去除重复的关键字
    for key in ori_key_words:
        if key not in key_words:
            key_words.append(key)
    print("共有%d个关键字"%len(key_words))

    Android_apis=get_Android_api()
    print("获取Android API信息完成...")
    sensitive_api = []
    num=0
    with open("pre_sensitive_api.txt","w",encoding='UTF-8') as sensitive_api_file:
        for ori_key_word in key_words:
            if len(ori_key_word)<3:
                continue
            num=num+1
            key_word=ori_key_word.lower()
            print("开始匹配第%d个关键词：%s"%(num,key_word))
            for api in Android_apis.keys():
                api_list=Android_apis[api]
                if key_word in api_list:
                    print("根据第%d个关键词%s匹配到的Android api为%s" % (num, ori_key_word, api))
                    if api not in sensitive_api:
                        sensitive_api.append(api)

        for api in sensitive_api:
            sensitive_api_file.write(api + "\n")
            sensitive_api_file.flush()



