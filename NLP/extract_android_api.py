
from bs4 import BeautifulSoup
import requests
import re

def get_packages_name():
    package_url = requests.get("https://developer.android.google.cn/reference/packages")
    package_html = BeautifulSoup(package_url.text, 'lxml')
    with open("Android_packages.txt","a+",encoding='UTF-8') as Android_packages_file:
        for package_info in package_html.find_all("td",attrs={"class":"jd-linkcol"}):
            Android_packages_file.write(package_info.get_text())

def get_classes_name(package_name):
    try:
        with open("Android_classes.txt","a+",encoding='UTF-8') as Android_classes_file:
            package_url=requests.get("https://developer.android.google.cn/reference/"+package_name+"/package-summary.html")
            package_html = BeautifulSoup(package_url.text, 'lxml')
            class_label=package_html.find("div", attrs={"class": "data-reference-resources-wrapper"})
            class_label_list=class_label.find_all("h2",attrs={"class":"hide-from-toc"})
            for label in class_label_list:
                if label.get_text()=="Classes":
                    classes=label.parent
                    for classes_info in classes.find_all("li"):
                        Android_classes_file.write(package_name + "/" + classes_info.get_text() + "\n")
                        Android_classes_file.flush()
    except AttributeError:
        return

def get_mtehods_info(class_name):
    with open("Android_API.txt","a+",encoding='UTF-8') as Android_API_file:
        class_url = requests.get("https://developer.android.google.cn/reference/"+class_name+".html")
        class_html = BeautifulSoup(class_url.text, 'lxml')
        if class_html.find("table",attrs={"id":"pubmethods"})==None:
            return
        method_info_list=class_html.find("table",attrs={"id":"pubmethods"}).find_all("td")
        return_value=""
        method_name=""
        describe_info=""
        for index in range(0,len(method_info_list)):
            if index%2==0:#说明是返回值
                temp_return_value=return_value+method_info_list[index].get_text().strip().replace("\n","")
                return_value=re.sub(' +', ' ', temp_return_value)
            else:#说明是方法名与描述信息
                method_name=method_name+method_info_list[index].find("code").get_text().strip().replace("\n","")
                if method_info_list[index].find("p")!=None:#描述信息可能没有
                    describe_info=describe_info+method_info_list[index].find("p").get_text().strip().replace("\n","")
                api_info=class_name+"."+method_name+return_value+"("+describe_info+")"
                Android_API_file.write(api_info+"\n")
                Android_API_file.flush()
                return_value = ""
                method_name = ""
                describe_info = ""

if __name__ == "__main__":
    #爬取包名
    get_packages_name()

    #爬取每个包下的类名
    with open("Android_packages.txt","r",encoding='UTF-8') as Android_packages_file:
        for line in Android_packages_file.readlines():
            package_name = line.strip("\n").replace(".", "/")
            get_classes_name(package_name)

    #爬取每个类下的api信息
    with open("Android_classes.txt", "r",encoding='UTF-8') as Android_classes_file:
        for line in Android_classes_file.readlines():
            class_name = line.strip("\n")
            get_mtehods_info(class_name)