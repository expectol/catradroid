#计算描述Android恶意行为的句子中的每个词的TF-IDF值，并由高到低排序
import nltk
import math
import string

from nltk.corpus import stopwords
from collections import Counter
from nltk.stem.porter import*

#PowerManager$WakeLock
#分词函数，对每个句子进行分词处理
def get_tokens(text):
    lower = text.lower()
    remove_punctuation_map = dict((ord(char), " ") for char in string.punctuation.replace("$",""))
    no_punctuation = lower.translate(remove_punctuation_map)
    tokens = nltk.word_tokenize(no_punctuation)

    return tokens

#对每个分词进行词干抽取
def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))

    return stemmed

#实现计算TF-IDF值
def calc_tf(word, count):
    return count[word] / sum(count.values())
def n_containing(word, count_list):
    return sum(1 for count in count_list if word in count)
def calc_idf(word, count_list):
    return math.log(len(count_list)) / (1 + n_containing(word, count_list))
def tfidf(word, count, count_list):
    return calc_tf(word, count) * calc_idf(word, count_list)

#判断字符串是否只包含数字
def not_contain_number(s):
    #pattern = re.compile('[0-9]+')
    #return not pattern.findall(s)
    return not s.isdigit()

def count_term(text):
    #对句子分词
    tokens = get_tokens(text)
    #过滤掉停用词
    pre_filtered = [w for w in tokens if not w in stopwords.words('english')]
    #过滤掉数字
    filtered=[w for w in pre_filtered if not_contain_number(w)]
    #Counter函数直接统计列表中每个词出现的次数,并按出现次数由高到低排序，返回元组Counter({2: 3, 1: 2, 3: 1, 4: 1})
    count = Counter(filtered)
    return count

def main():
    #恶意行为库文件，计算库中词的TF-IDF值，并排序追加写入sensitive_key.txt
    malicious_behavior_lib_file="malicious_behavior_lib.txt"

    texts = []
    with open(malicious_behavior_lib_file, "r",encoding='UTF-8') as malicious_behavior_lib_file:
        for line in malicious_behavior_lib_file.readlines():
            if line!="":
                line=line.strip("\n")
                texts.append(line)
    countlist = []
    for text in texts:
        #统计每个句子中的词频，返回元组形式({word: 3, language: 2, computer: 1, study: 1})
        countlist.append(count_term(text))
    all_scores={}
    for i, count in enumerate(countlist):
        scores = {word: tfidf(word, count, countlist) for word in count}
        all_scores.update(scores)
    sorted_words = sorted(all_scores.items(), key=lambda x: x[1], reverse=True)
    with open("sensitive_key.txt","w",encoding='UTF-8') as sensitive_key_file:
        for word, score in sorted_words:
            sensitive_key_file.write(word+"\n")

if __name__ == "__main__":
    main()
