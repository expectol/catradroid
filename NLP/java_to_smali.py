#将获取的java方法格式转换成smali方法格式

# 1、将包类中的/转换成.
# 2、参数与返回值中的类型格式转换：
# （1）基本类型格式转换：根据创建的转换字典来转换
# （2）引用类型格式转换：用类名称去java_classes文件或者Android_classes文件中找出对应的完整类名
# 		注意：引用类型完整类名前要加上L，类名后要加上;
# 3、最后给整个api套上<>

def get_java_class_map():
    java_class_map={}
    with open("java_classes.txt","r") as java_classes_file:
        for line in java_classes_file.readlines():
            line=line.strip("\n")
            class_name=line[line.rfind("/")+1:]
            if class_name not in java_class_map:
                java_class_map[class_name]="L"+line+";"
            inter_class_name=line.replace("/",".")
            if inter_class_name not in java_class_map:
                java_class_map[inter_class_name]="L"+line+";"
    return java_class_map

def get_Android_class_map():
    Android_class_map={}
    with open("Android_classes.txt","r") as Android_classes_file:
        for line in Android_classes_file.readlines():
            line=line.strip("\n")
            class_name=line[line.rfind("/")+1:]
            if class_name not in Android_class_map:
                Android_class_map[class_name]="L"+line+";"
            inter_class_name = line.replace("/", ".")
            if inter_class_name not in Android_class_map:
                Android_class_map[inter_class_name] = "L" + line + ";"
    return Android_class_map

def convert_one(ori_type):
    convert_value = ""
    if ori_type in type_map:
        convert_value = convert_value + type_map[ori_type]
    elif ori_type in Android_class_map:
        convert_value = convert_value + Android_class_map[ori_type]
    elif ori_type.startswith("java.") or ori_type.startswith("android."):
        convert_value = convert_value + "L" + ori_type.replace(".", "/") + ";"
    # 处理自定义类型数组
    elif ori_type.find("[]") != -1:
        temp = ori_type[:ori_type.find("[")]
        if temp in Android_class_map:
            convert_value = convert_value + "[" + Android_class_map[temp]
        else:
            convert_value = convert_value + "[" + temp[:1]
    else:
        convert_value=convert_value+ori_type
    return convert_value

def convert_type(ori_type,num):
    if ori_type.find("&#")!=-1 and ori_type.find(";")!=-1:
        result_info=""
        ori_type_list=ori_type.split(";")
        for ori_type in ori_type_list:
            ori_type=ori_type[:ori_type.find("&#")]
            result=convert_one(ori_type)
            result_info=result_info+result
        return return_info

    else:
        convert_value = ""
        if ori_type in type_map:
            convert_value = convert_value + type_map[ori_type]
        elif ori_type in Android_class_map:
            convert_value = convert_value + Android_class_map[ori_type]
        elif ori_type.startswith("java.") or ori_type.startswith("android."):
            convert_value=convert_value+"L"+ori_type.replace(".","/")+";"
        # 处理自定义类型二维数组
        elif ori_type.find("[][]") != -1:
            temp = ori_type[:ori_type.find("[")]
            if temp in Android_class_map:
                convert_value = convert_value + "[[" + Android_class_map[temp]
            else:
                convert_value = convert_value + "[[" + temp[:1]
        #处理自定义类型一维数组
        elif ori_type.find("[]")!=-1:
            temp=ori_type[:ori_type.find("[")]
            if temp in Android_class_map:
                convert_value=convert_value+"["+Android_class_map[temp]
            else:
                convert_value=convert_value+"["+temp[:1]
        return convert_value

if __name__=="__main__":
    type_map = {"void": "V", "boolean": "Z", "Byte": "B", "byte":"B","short": "S", "char": "C", "int": "I", "long": "J",
                "float": "F", "double": "D","int[]":"[I","byte[]":"[B","float[]":"[F","String[]":"[Ljava/lang/String;",
                "java.lang.String[]":"[Ljava/lang/String;","android.view.View":"Landroid/view/View;","char[]":"[C",
                "long[]":"[J","View":"Landroid/view/View;","string":"Ljava/lang/String;","view":"Landroid/view/View;"}
    #java_class_map=get_java_class_map()
    Android_class_map=get_Android_class_map()
    #读取文件并进行转换写入新的文件
    global error_num
    global add_class
    add_class=[]
    error_num=0
    num=0
    with open("pre_sensitive_api.txt","r",encoding="UTF-8") as sensitive_api_file:
        with open("sensitive_smali_api.txt", "w",encoding="UTF-8") as convert_sensitive_api_file:
            for line in sensitive_api_file.readlines():
                num=num+1
                line=line.strip("\n")
                #去掉api后面的描述信息
                if line.count("(")>=2:
                    one_index=line.find("(")
                    remain_line=line[one_index+1:]
                    two_index=remain_line.find("(") + one_index + 1
                    line=line[:two_index]
                #处理包名类名部分
                pre_info=line[:line.find("(")].replace("/",".")
                #处理返回值部分
                return_info=line[line.rfind(")")+1:]
                return_value=""
                if return_info.find(" ")!=-1:
                    return_value=return_value+return_info[return_info.find(" ")+1:]
                else:
                    return_value=return_value+return_info
                return_value=convert_type(return_value,num)
                #处理参数部分
                parameter_info=line[line.find("(")+1:line.find(")")]
                parameter_value=""
                if parameter_info!="":
                    parameter_list=parameter_info.split(", ")
                    for parameter in parameter_list:
                        class_name=parameter[:parameter.find(" ")]
                        parameter_value=parameter_value+convert_type(class_name,num)
                #最后套上<>
                smali_method="<"+pre_info+"("+parameter_value+")"+return_value+">"
                convert_sensitive_api_file.write(smali_method+"\n")






