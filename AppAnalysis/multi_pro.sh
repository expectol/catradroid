#!/bin/bash
./gradlew build
apk_set=/home/chenjun/IntelliDroid-master/IntelliDroid-master/AppAnalysis/preprocess/1/*
echo $apk_set
thread_num=5
for apk in ${apk_set} | xargs -n 1 -I {} -P ${thread_num} sh -c "./IntelliDroidAppAnalysis -o /home/chenjun/benign_result/${apk##*/} $apk"
