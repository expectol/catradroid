#!/bin/bash

PREPROCESS_DATASET=$(dirname $0)/PreprocessDataset.sh

APK_DIR=$1

for APK_FAMILY in $APK_DIR/*
do
    if [ -d "${APK_FAMILY}" ]; then
        ${PREPROCESS_DATASET} ${APK_FAMILY}
    fi
done
#调用预处理数据集脚本处理APK族
#循环处理APK—DIR中的APK
#脚步处理

