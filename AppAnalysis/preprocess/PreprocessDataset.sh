#!/bin/bash

source $(dirname $0)/IntelliDroidPreprocessAPK.sh

APK_DIR=$1
idx=0
for APK in ${APK_DIR}/*
do
    #APK_PATH=$(readlink -f "$APK")

    if [ -f "${APK}" ]; then
	let "idx += 1"
	echo "$idx apk"
        preprocessAPK ${APK}
    elif [ -d "$APK" ]; then
	#let "idx += 1"
	#echo "$idx apk"
        preprocessAPKDir ${APK}
    fi
done

#源程序是IntelliDroid.sh脚本
#对APK-DIR中的APK文件进行处理
#预处理直接预处理APK文件
#或者使用预处理APK路径处理APK
