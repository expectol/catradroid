var ykAPI = {
	initSwf: function(fname, opt) {
		if (typeof(opt) == 'undefined') opt = {};
		var dim = this.getScreenDimension();
		var w = opt['w'] || dim[0];
		var h = opt['h'] || dim[1];
		var tag = '<embed src="./'+fname+'" width="'+w+'" height="'+h+'" allowFullScreen="false" fullScreenEnable="false" fullScreenOnSelection="false"></embed>';
		$(tag).appendTo('body');
	},
	getScreenDimension: function() {
		return window.YGJsLib.getScreenDimension();
	},
	getAppData: function(key) {
		return window.YGJsLib.getAppData(key);
	},
	setAppData: function(key, value) {
		return window.YGJsLib.setAppData(key, value);
	}
};
