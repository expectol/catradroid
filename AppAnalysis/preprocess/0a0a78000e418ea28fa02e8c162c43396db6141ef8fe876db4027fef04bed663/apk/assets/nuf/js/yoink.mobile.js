﻿var touchstart = "mousedown";
var touchend = "mouseup";
var touchmove = "mousemove";
try {
    document.createEvent("TouchEvent");
    touchstart = "touchstart";
    touchend = "touchend";
    touchmove = "touchmove";
} catch (e) {
}  

(function( $ ) {
  $.fn.bindFirst = function(name, fn) {
      // bind as you normally would
      // don't want to miss out on any jQuery magic
      this.bind(name, fn);
      var handlers = this.data('events')[name];
      // take out the handler we just inserted from the end
      var handler = handlers.splice(handlers.length - 1)[0];
      // move it at the beginning
      handlers.splice(0, 0, handler);
  };

  $.fn.attachMobileLink = function() {
    mobileLite.attachLink(this);
    this.bind(touchstart, function() {
            $(this).addClass('hover');
          })
        .bind(touchend, function() {
            $(this).removeClass('hover');
          });
  }
  
  $.fn.styleCheckbox = function(checkbox) {
    checkbox.hide();
    var checkboxDiv = this;
    function updateDiv()
    {
      if (checkbox.attr("checked")) {
        checkboxDiv.css("backgroundPosition", "0 -"+(2*checkboxDiv.height())+"px");
      } else {
        checkboxDiv.css("backgroundPosition", "0 -"+(0*checkboxDiv.height())+"px");
      }
    }
    updateDiv();
    $(this).bind(touchstart, function() {
      if (checkbox.attr("checked")) {
        checkboxDiv.css("backgroundPosition", "0 -"+(3*checkboxDiv.height())+"px");
      } else {
        checkboxDiv.css("backgroundPosition", "0 -"+(1*checkboxDiv.height())+"px");
      }
    });
    $(document).bind(touchend, function(e) {
      if (e.target == checkboxDiv.get(0)) {
        if (checkbox.attr("checked")) {
          checkbox.removeAttr("checked");
        } else {
          checkbox.attr("checked", "checked");
        }
        updateDiv();
        checkbox.trigger("change");
      }
    })
    return $(this);
  }
  $.fn.styleRadio = function(radio) {
    radio.hide();
    var radioDiv = this;
    function updateDiv()
    {
      var radioVal = $("input[name='"+radio.attr("name")+"']:checked").val();
      $("input[name='"+radio.attr("name")+"']").each(function() {
        var div=$("div.radio", $(this).parent());
        if ($(this).val() == radioVal) {
          div.css("backgroundPosition", "0 -"+(3*div.height())+"px");
        } else {
          div.css("backgroundPosition", "0 -"+(0*div.height())+"px");
        }
      });
    }
    updateDiv();
    $(this).bind(touchstart, function() {
      var radioVal = $("input[name='"+radio.attr("name")+"']:checked").val();
      if (radio.val() == radioVal) {
        radioDiv.css("backgroundPosition", "0 -"+(2*radioDiv.height())+"px");
      } else {
        radioDiv.css("backgroundPosition", "0 -"+(1*radioDiv.height())+"px");
      }
    });
    $(document).bind(touchend, function(e) {
      if (e.target == radioDiv.get(0)) {
        //$("input[name='"+radio.attr("name")+"']").removeAttr("checked");
        radio.attr("checked", "checked");
      }
      updateDiv();
    });
    return $(this);
  }

  function daysInMonth(month, year)
  {
    return new Date(year, month, 0).getDate();
  }

  function stringToDate(str)
  {
    var match = str.match(/(\d+)\/(\d+)\/(\d+)/);
    return {year:match[3], month:match[1], day:match[2]};
  }

  function dateToString(month, day, year)
  {
    return month+'/'+day+'/'+year;
  }
  
  $.fn.styleDatePicker = function(dateInput, pageDiv) {
    var minYear = 1950;
    var scrollers = {year:null, month:null, day:null};
    var pickers = {year:null, month:null, day:null};

    function updateDatePicker(month, day, year)
    {
      scrollers.year.scrollTo(0, scrollers.year.gridHeight*(year-minYear));
      scrollers.month.scrollTo(0, scrollers.month.gridHeight*(month-1));
      var daysCount = daysInMonth(month, year);
      if ($(".daysList div", pickers.day).length != daysCount + 2) {
        // update days
        var wrapper = $(".daysList", pickers.day);
        wrapper.html('');
        wrapper.append("<div>DD</div>");
        for (var i=1; i<=daysCount; i++) {
          var padding = i<10?'0':'';
          wrapper.append("<div>"+padding+i+"</div>");
        }
        wrapper.append("<div>&nbsp;</div>");
        scrollers.day.setupScroller();
      }
      if (day <= 0) {
        day = 1;
      } else if (day > daysCount) {
        day = daysCount;
      }
      scrollers.day.scrollTo(0, scrollers.day.gridHeight*(day-1));
      dateInput.val(dateToString(month, day, year));
    }

    $(".datePickerScroller", this).each(function() {
        var wrapper = $(".datePickerScrollerWrapper", this);
        var scrollerDiv = $(this);
        var type;
        
        if (scrollerDiv.hasClass('yearPicker')) {
          var curDate = new Date();
          wrapper.append("<div>YEAR</div>");
          for (var i=minYear; i<=curDate.getFullYear(); i++) {
            wrapper.append("<div>"+i+"</div>");
          }
          wrapper.append("<div>&nbsp;</div>");
          pickers.year = scrollerDiv;
        } else if (scrollerDiv.hasClass('monthPicker')) {
          wrapper.append("<div>MM</div>");
          for (var i=1; i<=12; i++) {
            var padding = i<10?'0':'';
            wrapper.append("<div>"+padding+i+"</div>");
          }
          wrapper.append("<div>&nbsp;</div>");
          pickers.month = scrollerDiv;
        } else if (scrollerDiv.hasClass('dayPicker')) {
          var d = stringToDate(dateInput.val());
          var html = "<div>DD</div>";
          for (var i=1; i<=daysInMonth(d.month, d.year); i++) {
            var padding = i<10?'0':'';
            html = html + "<div>"+padding+i+"</div>";
          }
          html = html + "<div>&nbsp;</div>";
          wrapper.append("<div class='daysList'>"+html+"</div>");
          pickers.day = scrollerDiv;
        }
        
        var scroller = new TouchScroll(wrapper.get(0),
                                       {verticalScrollBar:$(".datePickerSliderKnob", scrollerDiv).get(0),
                                        verticalScrollTrack:$(".datePickerSliderBar", scrollerDiv).get(0),
                                        snapToGrid:true,
                                        gridHeight:parseInt(wrapper.css('line-height')),
                                        onSnapToGrid: function(axis, segment) {
                                          if (axis == "f") {
                                            var d = stringToDate(dateInput.val());
                                            var year = d.year, month = d.month, day = d.day;

                                            if (scrollerDiv.hasClass('yearPicker')) {
                                              year = minYear + segment;
                                            } else if (scrollerDiv.hasClass('monthPicker')) {
                                              month = segment + 1;
                                            } else if (scrollerDiv.hasClass('dayPicker')) {
                                              day = segment + 1;
                                            }
                                            updateDatePicker(month, day, year);
                                          }
                                        }
                                       });


        if (scrollerDiv.hasClass('yearPicker')) {
          scrollers.year = scroller;
        } else if (scrollerDiv.hasClass('monthPicker')) {
          scrollers.month = scroller;
        } else if (scrollerDiv.hasClass('dayPicker')) {
          scrollers.day = scroller;
        }
    });

    $(pageDiv).bind("pageShow", function() {
      var d = stringToDate(dateInput.val());
      scrollers.year.setupScroller();
      scrollers.month.setupScroller();
      scrollers.day.setupScroller();
      updateDatePicker(d.month, d.day, d.year);
    });

    $(pageDiv).bind("getDate", function(event, callback) {
        var year = -Math.floor(scrollers.year._currentOffset.f/scrollers.year.gridHeight)+minYear;
        var month = -Math.floor(scrollers.month._currentOffset.f/scrollers.month.gridHeight)+1;
        var day = -Math.floor(scrollers.day._currentOffset.f/scrollers.day.gridHeight)+1;
        callback(month, day, year);
        });
    return $(this);
  }

  function keyboardTextFieldChange(textField, inputField)
  {
    var hiddenTextField, showTextField;
    textField.each(function() {
      if ($(this).hasClass('hidden')) {
        hiddenTextField = $(this);
      } else {
        showTextField = $(this);
      }
    });
    setTimeout(function() {
        if (hiddenTextField.get(0).scrollWidth > inputField.width()) {
          showTextField.css('margin-left', (inputField.width() - hiddenTextField.get(0).scrollWidth)+'px');
        } else {
          showTextField.css('margin-left', '0px');
        }
      }, 1);
  }

  function keyboardUpdateTextFieldFromValue(textField, isPassword, blinkPassword)
  {
    var text = '';
    var length = (!!textField.attr('value'))?textField.attr('value').length:0;
    var textSpan = $('span.text', textField);
    if (isPassword) {
      for (var i = 0; i< length; i++) {
        if (blinkPassword && i+1 == length) {
          if (!!textField.attr('value')) {
            text = text + textField.attr('value')[i];
          }
        } else {
          text = text + '•';
        }
      }
    } else {
      if (!!textField.attr('value')) {
        text = textField.attr('value');
      } else {
        text = '';
      }
    }
    textSpan.text(text);
    textSpan.html(textSpan.html().replace(/ /g,"&nbsp;"));
    if (isPassword && blinkPassword) {
      setTimeout(function() {
        var text2;
        text2 = textSpan.first().text();
        if (text2.length >= length) text2 = text2.substr(0, length-1)+'•'+text2.substr(length);
        textSpan.text(text2);
      },1000);
    }
  }

  function switchKeyboardCase(kb, uppercase)
  {
    var capKey = $('.capKey', kb);
    if (uppercase) {
      if (capKey.hasClass('BTNDarkGreyCap')) {
        $('.capKey', kb).removeClass('BTNDarkGreyCap').addClass('BTNDarkGreyCapSelected');
        $('.letterPad a', kb).each(function() {
            if ($(this).text().length == 1 || $(this).text().toLowerCase() == '.com') {
              $(this).text($(this).text().toUpperCase());
            }
        });
      }
    } else {
      if (capKey.hasClass('BTNDarkGreyCapSelected')) {
        $('.capKey', kb).addClass('BTNDarkGreyCap').removeClass('BTNDarkGreyCapSelected');
        $('.letterPad a', kb).each(function() {
            if ($(this).text().length == 1 || $(this).text().toLowerCase() == '.com') {
              $(this).text($(this).text().toLowerCase());
            }
        });
      }
    }
  }

  function switchKeyboardPad(kb, pad)
  {
    $(".KeyboardPanelContainer > div", kb).hide();
    $(".KeyboardPadSwitch > a", kb).hide();
    if (pad == 'num') {
      $(".numPad").show();
    } else if (pad == 'sym') {
      $(".symPad").show();
    } else if (pad == 'letter') {
      $(".letterPad").show();
    }
    if (pad != 'letter') {
      switchKeyboardCase(kb, false);
    }
  }

	var keyboardCurrentInputField = null;
  var keyboardMethods = {
    init : function() {
        var kb = $(this);
        kb.hide();
        var inputField = $('.KeyboardInputField', kb);
        var textField = $('.KeyboardInputText', kb); 

        var statusDown = false;
        $('a', kb).bind(touchstart, function() {
          if (!statusDown) {
            $(this).addClass('hover');
            statusDown = true;
          }
        });
        $('a', kb).bind(touchend+' mouseout', function() {
          $('a.hover', kb).removeClass('hover');
          statusDown = false;
        });
        $('.KeyboardInputText .text, .KeyboardInputText', kb).click(function() {
          return false;
        });
      },
    show: function(input, label) {
        var kb = $(this);
    		if (keyboardCurrentInputField != null) {
					kb.hide(keyboardCurrentInputField);    			
    		}
    		keyboardCurrentInputField = input;
        $('.KeyboardTitle', kb).text(label);
        var inputField = $('.KeyboardInputField', kb);
        var textField = $('.KeyboardInputText', kb); 
        
        textField.attr('value',input.val());
        keyboardUpdateTextFieldFromValue(textField, input.attr('type')=='password', false);
        keyboardTextFieldChange(textField, inputField);
       
        switchKeyboardPad(kb, 'letter');

        kb.show();
        $('a', kb).bind(touchstart+'.kbclick', function() {
          var val = textField.attr('value');
          var blinkPassword = false;
          if ($(this).text().length == 1 || $(this).text().toLowerCase() == '.com') {
            textField.attr('value', val + $(this).text());
            blinkPassword = true;
          } else if ($(this).hasClass('spaceKey')) {
            textField.attr('value', val + ' ');
            blinkPassword = true;
          } else if ($(this).hasClass('backspaceKey')) {
            if (val.length > 0) {
              textField.attr('value', val.substr(0, val.length-1));
            }
          } else if ($(this).hasClass('capKey')) {
            if ($(this).hasClass('BTNDarkGreyCap')) {
              switchKeyboardCase(kb, true);
            } else if ($(this).hasClass('BTNDarkGreyCapSelected')) {
              switchKeyboardCase(kb, false);
            }
          } else if ($(this).hasClass('numPadKey')) {
            switchKeyboardPad(kb, 'num');
          } else if ($(this).hasClass('symPadKey')) {
            switchKeyboardPad(kb, 'sym');
          } else if ($(this).hasClass('letterPadKey')) {
            switchKeyboardPad(kb, 'letter');
          }
          if (val != textField.attr('value')) {
            keyboardUpdateTextFieldFromValue(textField, input.attr('type')=='password', blinkPassword);
            keyboardTextFieldChange(textField, inputField);
          }
        });
       
        $('.doneKey', kb).bind('click.done', function() {
          kb.keyboard('hide', input);
        });

      },
    hide: function(input) {
        var kb = $(this);
        if (!input) {
        	input = keyboardCurrentInputField;
        }
       	keyboardCurrentInputField = null;
        $('.doneKey', kb).unbind('click.done');
        var textField = $('.KeyboardInputText', kb);
        if (!!input) input.val(textField.attr('value'));
        $('a', kb).unbind(touchstart+'.kbclick');
        kb.hide();
        if (!!input) input.trigger('change');
      }
  };

  $.fn.keyboard = function(method) {
    // Method calling logic
    if ( keyboardMethods[method] ) {
      return keyboardMethods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return keyboardMethods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on keyboard' );
    }    
  };

  function updateDivFromInput(div, input)
  {
    var text = '';
    if (input.attr('type') == 'password') {
      for (var i=0;i<input.val().length;i++) {
        text = text + '•';
      }
      if (text.length <= 0) {
        text = ' ';
      }
    } else {
      text = input.val();
      if (text.length == 0) {
        text = input.attr('placeholder');
        if (text.length <= 0) {
          text = ' ';
        }
      }
    }
    div.text(text);
    div.html(div.html().replace(/ /g, "&nbsp;"));
  }

  $.fn.styleInput = function(keyboard) {
    this.each(function() {
      var wrapper = $(this);
      var input = $('input', wrapper);
      var inputDiv = $('div.input', wrapper);
      updateDivFromInput(inputDiv, input);
      inputDiv.bind(touchend, function() {
        setTimeout(function() {
          keyboard.keyboard('show', input, input.attr('label'));
        }, 100);
      });
      inputDiv.click(function() {
        return false;
      });
      input.bind('change', function() {
        updateDivFromInput(inputDiv, input);
      });
    });
  };
})( jQuery );

mobileLite = new function(){
  this.defaultTransition = 'slide';

  var pageStack = new Array();
  
  this.setOptions = function(myopt)
  {
    $.extend(mobileLite, myopt);
  }
  
  var targetDPI = 240;			// default is high-dpi
  var targetDPIScale = 1.5;	// also for high-dpi
  var actualDPI = 240;			// high-dpi
  this.screenHeight = 800;
  this.screenWidth = 480;
  this.screenTopPadding = 0;
  this.screenLeftPadding = 0;
  this.writeViewport = function(dpi, scale, width, height)
  {
  	targetDPI = dpi;
  	targetDPIScale = scale;
  	targetMediumWidth = Math.round(width/scale);
  	targetMediumHeight = Math.round(height/scale);
  	
  	DPIScale = window.devicePixelRatio;
  	innerHeight = (!!window.innerHeight?window.innerHeight:320);
  	innerWidth = (!!window.innerWidth?window.innerWidth:533);
  	
  	DPIHeight = Math.floor(dpi*height/scale/innerHeight);
    DPIWidth = Math.floor(dpi*width/scale/innerWidth);
    if (DPIWidth >= DPIHeight) {
    	// scale allong the width
  		actualDPI = DPIWidth;
  		mobileLite.screenWidth = width;
  		mobileLite.screenHeight = Math.ceil(width*innerHeight/innerWidth);
  		mobileLite.screenTopPadding = Math.floor((mobileLite.screenHeight - height)/2.0);
  		mobileLite.screenLeftPadding = 0;
  	} else {
    	// scale allong the height
  		actualDPI = DPIHeight;
  		mobileLite.screenHeight = height;
  		mobileLite.screenWidth = Math.ceil(height*innerWidth/innerHeight);
  		mobileLite.screenLeftPadding = Math.floor((mobileLite.screenWidth - width)/2.0);
  		mobileLite.screenTopPadding = 0;
  	}
  	//console.log("Screen size: "+mobileLite.screenWidth+" "+mobileLite.screenHeight);
  	//console.log("Screen padding: "+mobileLite.screenLeftPadding+" "+mobileLite.screenTopPadding);
		document.write('<meta name="viewport" content="target-densitydpi='+actualDPI+', width=device-width, user-scalable=no, initial-scale = 1.0" />');
  }
  
  this.changePage = function(fromPage, toPage, transition, reverse, 
                             pushPageToStack)
  {
    /*
    toPage.removeClass("hidden-page").offset({top:0, left:0}).show();
    fromPage.hide();
            if (pushPageToStack) {
              pageStack.push({  // page pushed here
                page:toPage,
                transition: transition,
                reverse: reverse
              });
            }
            toPage.trigger("pageShow");
    return;
    */
    
    transition = typeof(transition) != 'undefined' ? 
                    transition:mobileLite.defaultTransition;

    reverse = typeof(reverse) != 'undefined' ? 
                    reverse:"";
    pushPageToStack = typeof(pushPageToStack) != 'undefined' ? 
                    pushPageToStack:true;

    toPage.show().offset({top:0, left:0})
          .removeClass("hidden-page")
          .addClass("ui-mobile-viewport-perspective ui-mobile-viewport-transitioning");
    fromPage.addClass("ui-mobile-viewport-perspective ui-mobile-viewport-transitioning");
    
    fromPage.addClass(transition+" out "+reverse);
    $(document).trigger('pageTransitionStart', [transition]);
    
    fromPage.trigger("pageHide", [fromPage]);

    toPage.addClass(transition+" in "+reverse)
          .one('webkitAnimationEnd', function(){
            fromPage.removeClass("in out reverse "+transition)
                    .hide();
            toPage.removeClass("in out reverse "+transition);
            if (pushPageToStack) {
              pageStack.push({  // page pushed here
                page:toPage,
                transition: transition,
                reverse: reverse
              });
            }
            toPage.trigger("pageShow", [toPage]);
            // fromPage.empty();    // do not evict pages
          });

  }
 
  function linkClick()
  {
    if (mobileLite.navigateTo($(this))) {
      return false;
    }
  }

  this.navigateTo = function(link, href)
  {
    var toPage;
    var reverse = "";
    var pushPageToStack = true; // false if it is back button
    var fromPage;
    
    var transition = mobileLite.defaultTransition;
    if (link.attr('data-transition')) {
      transition = link.attr('data-transition');
    }

    if (link.attr("data-rel") == "back" && pageStack.length >= 2) {
      fromPageObj = pageStack.pop();
      toPageObj = pageStack[pageStack.length-1];
      
      toPage = toPageObj.page;
      fromPage = fromPageObj.page;
      transition = fromPageObj.transition;
      reverse = fromPageObj.reverse=="reverse"? "":"reverse";
      
      pushPageToStack = false;
    } else {
      if (link.attr('data-direction') == 'reverse') {
        reverse = 'reverse';
      }

      fromPageObj = pageStack[pageStack.length - 1];
      fromPage = fromPageObj.page;
      
      var toId; 
      if (!!href) {
        toId = href.replace(/\.html/, '');
      } else {
        toId = link.attr('href').replace(/\.html/, '');
      }
      if (toId.match(/^https?:\/\//i)) {
      	return false;
      }
      var toPage = null;
      toId = toId.substr(toId.lastIndexOf('/')+1);
      if (toId) {
        $("div[data-role='page']").each(function(){
          if (toId == this.id) {
            toPage = $(this);
          }
        });
      }
      if (toPage == null && toId.match(/^[^\/#.]+$/)) {
        // create a new div[data-role="page"]
        toPage = $('<div data-role="page" id="'+toId+'"></div>');
        toPage.insertAfter($("div[data-role='page']").last());
        toPage.hide();
      }
    }
    if (toPage) {
      loadPage(toPage, function() {
        mobileLite.changePage(fromPage, toPage, transition, reverse, 
                              pushPageToStack);
      });
      return true;
    }
    return false;
  }
  
  function loadPage(pageDiv, callback)
  {
    if (pageDiv.html().trim() == "") {
      if (pageDiv.attr('loading')) {
        return; // do nothing, do not reload
      }
      pageDiv.attr('loading', true);
      $.get(
        pageDiv.attr('id') + '.html',
        function(data) {
          // hack to load html properly
          var html = $(data.replace(/(<\s*?body.*?>)/,"$1<div>")
                           .replace(/(<\/\s*?body.*?>)/,"</div>$1"));
          pageDiv.html($("div[data-role='page']", html).html());
          //pageDiv.width($(":first", pageDiv).width());
          //pageDiv.height($(":first", pageDiv).height());
          pageDiv.removeAttr('loading');

          mobileLite.attachLink($("a", pageDiv));
          callback(pageDiv);

					pageDiv.height(mobileLite.screenHeight);	 
					pageDiv.width(mobileLite.screenWidth);
					pageDiv.css("padding-left", mobileLite.screenLeftPadding+"px");	 
					pageDiv.css("padding-top", mobileLite.screenTopPadding+"px");	 
					pageDiv.css("background-position", 
											(mobileLite.screenLeftPadding)+"px "+
											(mobileLite.screenTopPadding)+"px");	 

          pageDiv.trigger("pageLoad", pageDiv);
        },
        'text'				
      );
    } else {
      callback(pageDiv);
      //pageDiv.trigger("pageLoad", pageDiv);
      //$(document).trigger("pageLoad", pageDiv);
    }
  }
  
  this.attachLink = function(elem)
  {
    elem.bind("click", linkClick);
  }
  
	this.initializePage = function() {
    $(document).bind("mobileLiteInit", function() {
      var pageCount = 0;
      var totalPageCount = $("div[data-role='page']").length;

      // Get default page from url hash
      var defaultPageIdx = 1;
      var hashId = null;
      if (!!location.hash) {
        hashId = location.hash.substr(1);
      } else if (!!sessionStorage.defaultPageId) {
        hashId = sessionStorage.defaultPageId;
      }

      if (!!hashId) {
        var pageidx = 0;
        $("div[data-role='page']").each(function(){
          pageidx ++;
          if ($(this).attr("id") == hashId) {
            defaultPageIdx = pageidx;
            return false;
          }
        });
      }

      $("div[data-role='page']").each(function(){
        pageCount ++;
        var currentPageIdx = pageCount;
        var pageDiv = $(this);

        pageDiv.addClass("hidden-page");
        
        var alreadyLoaded = (pageDiv.html().trim() != "");

				// Only load the first page
        if (currentPageIdx == defaultPageIdx) {
	        loadPage(pageDiv, function () {
	          
	          if (alreadyLoaded) {
							pageDiv.height(mobileLite.screenHeight);	 
							pageDiv.width(mobileLite.screenWidth);	 
							pageDiv.css("padding-left", mobileLite.screenLeftPadding+"px");	 
							pageDiv.css("padding-top", mobileLite.screenTopPadding+"px");	 

	            mobileLite.attachLink($("a", pageDiv));
	            pageDiv.trigger("pageLoad", pageDiv);
	          }
	          
	          if (currentPageIdx == defaultPageIdx) {
	            pageStack.push({
	                page: pageDiv, 
	                transition: mobileLite.defaultTransition,
	                reverse: ''
	            });
	            pageDiv.removeClass("hidden-page");
	            pageDiv.trigger("pageShow", [pageDiv]);
	          }
	          
	          // if (currentPageIdx == totalPageCount) {
	          //   $(document).trigger("allPageLoad");
	          // }
	          $(document).trigger("allPageLoad");
	        });
				}
      });
    });
    
    $(document).trigger("mobileLiteInit");
	};
};

/**
 * Logging
 */
$(document).bind("pageLoad", function(event, pageDiv) {
  // Adding log tag
  $("[data-logtag]", pageDiv).click(function() {
    var val = !!$(this).attr("data-logval")?$(this).attr("data-logval"):1;
    var tag = $(this).attr("data-logtag");
    window.YGJsLib.addLog(tag+"", val+"");
  });
});

