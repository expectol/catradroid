// Decode URI component and translate + to spaces
function decodeURIComponent2(str)
{
  if (str) {
    return decodeURIComponent(str.replace(/\+/g, " "));	// decode
  } else {
    return null;
  }
}

// Loaded success
var alreadyLoaded = false;
function onNUFLoadSucceed()
{
	if (!alreadyLoaded) {
		alreadyLoaded = true;
		window.YGJsLib.contentsLoaded();
	}	
}

// Tracking tags
var trackConfig = {
  errorScreenSize:{
  	tag: "nuf.error.screenSize",
  	enabled: true,
 	}
};

function YGJsLibAddLog(tag, value)
{
  if (!value) {
    value = "1";
  }
  // console.log('window.YGJsLib.addLog: '+tag);
  window.YGJsLib.addLog(tag, value);
}

// Output the meta data for viewport in order to do proper page scaling
if (!window.innerWidth || !window.innerHeight) {
	if (!!trackConfig.errorScreenSize.enabled) {
		YGJsLibAddLog(trackConfig.errorScreenSize.tag, 'Size: '+window.innerWidth+', '+window.innerHeight);
	}
}
mobileLite.writeViewport(240, 1.5, 800, 480);

// Play Sound
function playSound(type)
{
	// dummy now, disable sound
}

// Initialization
$(function() {
  mobileLite.initializePage();
  
	$("body").height(mobileLite.screenHeight);	 
	$("body").width(mobileLite.screenWidth);
  
  $(document).one("contentsFinished", function(event, yuser) {
      window.YGJsLib.contentsFinished(JSON.stringify(yuser));
      // end
  });

  window.YGJsLib.setInputEventListener("onAndroidInputEventListener");
});

function onAndroidInputEventListener(encodedData)
{
  var data = decodeURIComponent2(encodedData);
  var dataArray = data.split(',',3);

  if (parseInt(dataArray[0]) == 262144) {
    if ($('#thankyouScreen').is(':visible')) {
    	// No confirmation
      $(document).trigger("contentsFinished", [yuser]);
		}  
  }
}

// UI modification
$(document).bind("allPageLoad", function(event) {

	$("body div[data-role='popup'] a").bind(touchstart, function() {
      $(this).addClass('hover');
    })
    .bind(touchend, function() {
      $(this).removeClass('hover');
    })
    .bind(touchstart, function() {
      if ($(this).hasClass('returnGameBtn')) {
        playSound('exit');
      } else {
        playSound('click');
      }
	});
	
});

$(document).bind("pageTransitionStart", function(event, transition) {
    if (transition == "slide") {
      playSound('transition');
    }
});

$(document).bind("pageLoad", function(event, pageDiv) {
  $("a", pageDiv).bind(touchstart, function() {
      $(this).addClass('hover');
    })
    .bind(touchend, function() {
      $(this).removeClass('hover');
    })
    .bind(touchstart, function() {
      if ($(this).hasClass('returnGameBtn')) {
        playSound('exit');
      } else {
        playSound('click');
      }
    });
  
  if (!!window['yoinkJS'] && !!yoinkJS[$(pageDiv).attr("id")] && !!yoinkJS[$(pageDiv).attr("id")].onLoad) {
    yoinkJS[$(pageDiv).attr("id")].onLoad(pageDiv);
  }
});

if (!window['yoinkJS']) {
  window['yoinkJS'] = {};
}

var yoink_token = null;
var yid = null;
var yuser = {};

yoinkJS.thankyouScreen = {
  onLoad: function(pageDiv) {
      $(".returnGameBtn", pageDiv).click(function() {
        yuser.yid = yid;
        yuser.yoink_token = yoink_token;
        $(document).trigger("contentsFinished", [yuser]);
        // return to game
      });
      
      var img = new Image();
      $(img).load(function() {
      	onNUFLoadSucceed();
      });
      img.src = 'images/BG_CenterThankYou.jpg';
    }
};
