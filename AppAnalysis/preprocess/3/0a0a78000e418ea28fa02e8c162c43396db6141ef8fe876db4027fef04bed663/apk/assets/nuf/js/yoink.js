// Decode URI component and translate + to spaces
function decodeURIComponent2(str)
{
  if (str) {
    return decodeURIComponent(str.replace(/\+/g, " "));	// decode
  } else {
    return null;
  }
}

// NUF loaded success
var alreadyLoaded = false;
function onNUFLoadSucceed()
{
	if (!alreadyLoaded) {
		alreadyLoaded = true;
		window.YGJsLib.contentsLoaded();
	}	
}

// Entry point for tracking tags, default to unknown
var trackEntryPoint = 'unknown';

// Tracking tags
var trackTagPrefix = 'nuf';
var trackConfig = {
  startNoAds:{
    tag: "landing.noads",
    enabled: true,
    withEntryPoint: false,
  },
  startPostLB:{
    tag: "landing.postLB",
    enabled: true,
    withEntryPoint: false,
  },
  startPostToFB:{
    tag: "landing.postToFB",
    enabled: true,
    withEntryPoint: false,
  },
  startUnlock:{
    tag: "landing.unlock",
    enabled: true,
    withEntryPoint: false,
  },
  impression:{
    tag: "impression",
    enabled: true,
    withEntryPoint: true,    
  },
  fbsignup:{
    tag: "signup.fb",
    enabled: true,
    withEntryPoint: true,
  },
  emailsignup:{
    tag: "signup.email",
    enabled: true,
    withEntryPoint: true,
  },
  relogin:{
    tag: "relogin.button",
    enabled: true,
    withEntryPoint: true,
  },
  backNUF:{
    tag: "back",
    enabled: true,
    withEntryPoint: true,
  },
  exitNUF:{
    tag: "exit",
    enabled: true,
    withEntryPoint: true,
  },
  confirmExit:{
    tag: "exit.confirm",
    enabled: true,
    withEntryPoint: true,
  },
  ppview:{
    tag: "policy.view",
    enabled: true,
    withEntryPoint: true,
  },
  tosview:{
    tag: "tos.view",
    enabled: true,
    withEntryPoint: true,
  },
  nuftime:{
    tag: "timespent",
    interval: 30, // in seconds
    max: 600,
    enabled: true,
    withEntryPoint: true,
  },
  skipNUF:{
  	tag: "skip",
  	enabled: true,
    withEntryPoint: true,
 	},
  errorScreenSize:{
  	tag: "error.screenSize",
  	enabled: true,
    withEntryPoint: false,
 	}
};

function YGJsLibAddLog(tagConfig, value, subfix)
{
	if (!!tagConfig.enabled) {
	  if (!value) {
	    value = "1";
	  }
	  var tag = tagConfig.tag;
	  if (!!subfix) {
	  	tag = tag+'.'+subfix;
	 	}
	 	if (!!tagConfig.withEntryPoint) {
	 		tag = trackEntryPoint+'.'+tag;
	 	}
	  if (!!trackTagPrefix && trackTagPrefix.toString().length > 0) {
	  	tag = trackTagPrefix+'.'+tag;
	  }
	  window.YGJsLib.addLog(tag, value);
	}
}

// Output the meta data for viewport in order to do proper page scaling
if (!window.innerWidth || !window.innerHeight) {
	YGJsLibAddLog(trackConfig.errorScreenSize, 'Size: '+window.innerWidth+', '+window.innerHeight);
}
mobileLite.writeViewport(240, 1.5, 800, 480);

// Play Sound
function playSound(type)
{
	// dummy now, disable sound
}

// Initialization
$(function() {
  mobileLite.initializePage();
  
  $('#yoinkKeyboard').keyboard();
  $('#yoinkKeyboard a').bind(touchstart, function() {
    playSound('keyboard');
  });
 
	$("body").height(mobileLite.screenHeight);	 
	$("body").width(mobileLite.screenWidth);
	 
  var nuftimeIntId = 0;
  var nufStart = new Date();
  var nufTimeCount = 0;

  var nuftimeLog = function() {
      var currentTime = new Date();
      var curCount = Math.round((currentTime.getTime() - nufStart.getTime())
                                /1000/trackConfig.nuftime.interval);
      if (curCount > nufTimeCount && 
      		(!!trackConfig.nuftime.max && 
      		 curCount*trackConfig.nuftime.interval <= trackConfig.nuftime.max)) {
        nufTimeCount = curCount;
        YGJsLibAddLog(trackConfig.nuftime,1,
                      curCount*trackConfig.nuftime.interval);
      }
    };

  if (!!trackConfig.nuftime && !!trackConfig.nuftime.enabled) {
    nuftimeIntId = setInterval(nuftimeLog, trackConfig.nuftime.interval*1000);
  }
  
  $(document).one("contentsFinished", function(event, yuser) {
      if (nuftimeIntId > 0) {
        nuftimeLog();
        clearInterval(nuftimeIntId);
      }
      window.YGJsLib.contentsFinished(JSON.stringify(yuser));
      // end
  });

  window.YGJsLib.setInputEventListener("onAndroidInputEventListener");
});

// Error Popup
$(function() {
  var popup = $("#errorPopup");
  $(".closeBtn", popup).click(function() {
    popup.hide();
  });
});

function showErrorPopup(errors)
{
  var popup = $("#errorPopup");
  var msg = $(".PopupErrorMessage", popup);
  msg.empty();
  for(var i in errors) {
    var div = $("<div></div>");
    var s = "";
    for(var j in errors[i]) {
      if (s.length > 0) s = s+' ';
      s = s+errors[i][j];
    }
    div.text(s);
    msg.append(div);
  }
  popup.show();
}

// Waiting Popup
function animateWaitingPopup(div)
{
	return 0;
	
	var waitingSpriteTotalHeight = 4000;
	var waitingSpriteHeight = 50;
	var waitingSpriteY = 0;
	var id = setInterval(function() {
		waitingSpriteY = (waitingSpriteY + waitingSpriteHeight) % waitingSpriteTotalHeight;
		div.css('background-position', '0px -'+waitingSpriteY+'px');
	}, 40);
	return null;
}

var waitingPopupTimerId;
function showWaitingPopup()
{
  var popup = $("#waitingPopup");
  waitingPopupTimerId =
  	animateWaitingPopup($(".PopupWaitingSprite", popup));
  popup.show();
}

function hideWaitingPopup()
{
  $("#waitingPopup").hide();
  clearInterval(waitingPopupTimerId);
}

// Exit Popup
// callback(confirmExit), confirmExit=true, perform exit
function showExitPopup(callback)
{
  var popup = $("#exitPopup");
  
  $('.closeBtn', popup).one('click.popup', function() {
    closeExitPopup();
    callback(false);
  });
  $('.exitBtn', popup).one('click.popup', function() {
    closeExitPopup();
    callback(true);
  });
  
  popup.show();
}

function closeExitPopup()
{
  var popup = $("#exitPopup");
  popup.hide();
	$('.closeBtn', popup).unbind('click.popup');
	$('.exitBtn', popup).unbind('click.popup');
}

function trackSkipNUF(pageName)
{
 	if (!pageName) {
 		pageName = $("div[data-role='page']").filter(':visible').attr('id');
 	}
  YGJsLibAddLog(trackConfig.skipNUF, 1,pageName);
}

function trackBackNUF()
{
 	var pageName = $("div[data-role='page']").filter(':visible').attr('id');
  YGJsLibAddLog(trackConfig.backNUF, 1, pageName);
}

function trackExitNUF()
{
 	var pageName = $("div[data-role='page']").filter(':visible').attr('id');
  YGJsLibAddLog(trackConfig.exitNUF, 1, pageName);
}

function trackConfirmExit()
{
 	var pageName = $("div[data-role='page']").filter(':visible').attr('id');
  YGJsLibAddLog(trackConfig.confirmExit, 1, pageName);
}

function onAndroidInputEventListener(encodedData)
{
  var data = decodeURIComponent2(encodedData);
  var dataArray = data.split(',',3);

  if (parseInt(dataArray[0]) == 262144) {
  	if ($('#yoinkKeyboard').is(':visible')) {
  		// quit keyboard
  		$("#yoinkKeyboard").keyboard("hide");
    } else if ($('#registerPopup').is(':visible')) {
  		$('#registerPopup').hide();
    } else if ($('#errorPopup').is(':visible')) {
    	$('#errorPopup').hide();
    } else if ($('#waitingPopup').is(':visible')) {
    	hideWaitingPopup();
    } else if ($('#exitPopup').is(':visible')) {
    	closeExitPopup();
    } else if ($('#popupRegister').is(':visible')) {
    	// No confirmation
	    trackExitNUF();
    	trackConfirmExit();
      $(document).trigger("contentsFinished", [yuser]);
    } else if ($('#thankyouScreen').is(':visible')) {
    	// No confirmation
      $(document).trigger("contentsFinished", [yuser]);
  	} else {
  		trackExitNUF();
	    showExitPopup(function(confirmExit) {
	        if (!!confirmExit) {
	        	trackConfirmExit();
	        	if (!!yuser.display_name) {
	          	$(document).trigger("contentsFinished", [yuser]);
						} else {
	          	$(document).trigger("contentsFinished", [{}]);
						}	          	
	        }
	      });
		}  
  }
}

// Form validation
function updateFormValidationStatus(pageDiv, errors, fields, showLarge, hideGood)
{
  var success = true;
  for (var field in (!!fields?fields:errors))
  {
    var fieldErrors = errors[field];
    if (fieldErrors.length > 0) {
    	if (!!showLarge) {
	      $("."+field+"Result", pageDiv).addClass("resultBadLarge").removeClass("resultGoodLarge");
    	} else {
	      $("."+field+"Result", pageDiv).addClass("resultBad").removeClass("resultGood");
			}      
      success = false;
    } else {
    	if (!!hideGood) {
        $("."+field+"Result", pageDiv).removeClass("resultBadLarge").removeClass("resultBad");
    	} else {
	    	if (!!showLarge) {
		      $("."+field+"Result", pageDiv).removeClass("resultBadLarge").addClass("resultGoodLarge");
		    } else {
		      $("."+field+"Result", pageDiv).removeClass("resultBad").addClass("resultGood");
		    }
			}	    
    }
  }
  return success;
}

// UI modification
$(document).bind("allPageLoad", function(event) {
	$("body div[data-role='popup'] a").bind(touchstart, function() {
      $(this).addClass('hover');
    })
    .bind(touchend, function() {
      $(this).removeClass('hover');
    })
    .bind(touchstart, function() {
      if ($(this).hasClass('returnGameBtn')) {
        playSound('exit');
      } else {
        playSound('click');
      }
	});
	
	if ($('#popupRegister').is(':visible')) {
	  YGJsLibAddLog(trackConfig.startPostLB);			
	}
	if ($('#splashScreen').is(':visible')) {
  	YGJsLibAddLog(trackConfig.startNoAds);			
	}	
	if ($('#facebookConnect').is(':visible')) {
    YGJsLibAddLog(trackConfig.startPostToFB);			
	}
	if ($('#unlockLevel').is(':visible')) {
    YGJsLibAddLog(trackConfig.startUnlock);			
	}	
	
});

$(document).bind("pageTransitionStart", function(event, transition) {
    if (transition == "slide") {
      playSound('transition');
    }
});

$(document).bind("pageLoad", function(event, pageDiv) {
  $("a", pageDiv).bind(touchstart, function() {
      $(this).addClass('hover');
    })
    .bind(touchend, function() {
      $(this).removeClass('hover');
    })
    .bind(touchstart, function() {
      if ($(this).hasClass('returnGameBtn')) {
        playSound('exit');
      } else {
        playSound('click');
      }
    });
  
  $(".tosScroller", pageDiv).each(function() {
    var scroller = new TouchScroll($(".tosScrollerWrapper", this).get(0), 
                                  {verticalScrollBar:$(".tosSliderKnob", this).get(0),
                                   verticalScrollTrack:$(".tosSliderBar", this).get(0)});
    $(pageDiv).bind("pageShow", function() {
      scroller.setupScroller();
    });
  });
  $(".checkboxWrapper", pageDiv).each(function() {
    $("div.checkbox", this).styleCheckbox($("input[type='checkbox']", this))
                           .bind(touchstart, function() {
                             playSound('everythingElse');
                           });
  });
  $(".radioWrapper", pageDiv).each(function() {
    $("div.radio", this).styleRadio($("input[type='radio']", this))
                        .bind(touchstart, function() {
                          playSound('everythingElse');
                        });
  });
 
  $(".datePickerWidget", pageDiv).each(function() {
    $(this).styleDatePicker($("input[name='birthday']", this), pageDiv);
  });

  if (!!window['yoinkJS'] && !!yoinkJS[$(pageDiv).attr("id")] && !!yoinkJS[$(pageDiv).attr("id")].onLoad) {
    yoinkJS[$(pageDiv).attr("id")].onLoad(pageDiv);
  }

	$("a[data-rel='back']", pageDiv).click(function() {
		trackBackNUF();
	});

  $(".inputWrapper", pageDiv).styleInput($('#yoinkKeyboard'));
});

$(document).bind("pageShow", function(event, pageDiv) {
	if (trackEntryPoint == "unknown") {
		if (pageDiv.attr("id")=='popupRegister'){
			trackEntryPoint = 'postLB';
		} else if (pageDiv.attr("id")=='splashScreen'){
			trackEntryPoint = 'noads';
		} else if (pageDiv.attr("id")=='facebookConnect'){
			trackEntryPoint = 'postToFB';
		} else if (pageDiv.attr('id')=='unlockLevel') {
			trackEntryPoint = 'unlock';
		}	
	}	
	
  if (!!$(pageDiv).attr("id")) {
    var pageName = $(pageDiv).attr("id");
    YGJsLibAddLog(trackConfig.impression, 1, pageName);
  }
  if (!!window['yoinkJS'] && !!yoinkJS[$(pageDiv).attr("id")] && !!yoinkJS[$(pageDiv).attr("id")].onShow) {
    yoinkJS[$(pageDiv).attr("id")].onShow(pageDiv);
  }
});

if (!window['yoinkJS']) {
  window['yoinkJS'] = {};
}

var yoink_token = null;
var yid = null;
var yuser = {};
var loginType = "facebook"; // either facebook or email

function onFacebookLogin(encodedJSON)
{
  var json = decodeURIComponent2(encodedJSON);
  var data = JSON.parse(json);
  hideWaitingPopup();
  if (!!data.statusCode) {
  	if (data.statusCode == 1002) {	// facebook cancel
			if ($('#facebookConnect').is(':visible')) {
		    trackExitNUF();
		    trackConfirmExit();
		    $(document).trigger("contentsFinished", [{}]);
		    return;
			} else {
	  		// do nothing
		 		return;
			}	 		
  	} else if (data.statusCode == 1001) {	// disconnected	
  		showErrorPopup({'network':['Network is disconnected, please check settings.']});
	 		return;
  	} else if (data.statusCode >= 300){
  		showErrorPopup({'facebook':['Facebook login error.']});
  		return;
  	}
  }

	yoink_token = data.yoink_token;
	yid = data.yid;
	yuser = {yid: data.yid, yoink_token:data.yoink_token};
	if (!!data.display_name) {
		yuser.display_name = data.display_name;
	} else {
		yuser.display_name = null;
	}
	if (!!data.gender) yuser.gender = data.gender;
	if (!!data.first_name) yuser.first_name = data.first_name;
	if (!!data.last_name) yuser.last_name = data.last_name;
	if (!!data.birthday) yuser.birthday = data.birthday;

  if (!!data.display_name && data.display_name.toString().length > 0) {
    YGJsLibAddLog(trackConfig.relogin);

    $(document).trigger("contentsFinished", [yuser]);
    // return to game
  } else {
    YGJsLibAddLog(trackConfig.fbsignup);

		setTimeout(function() {
	    mobileLite.navigateTo($(".facebookBtn").first(), "tosAgreement.html");
		}, 1);
  }
}

yoinkJS.popupRegister = {
  onLoad: function(pageDiv) {
      $('.closeBtn', pageDiv).click(function() {
		    trackExitNUF();
		    trackConfirmExit();
        // Disable confirmation
        // showExitPopup(function(confirmExit) {
        //     if (!!confirmExit) {
	      //		   trackConfirmExit();
        //       $(document).trigger("contentsFinished", [{}]);
        //     }
        //   });
        $(document).trigger("contentsFinished", [{}]);
        // return to game
      });
      $(pageDiv).bind("pageHide", function() {
          $("body").removeClass("transparent");
        });
        
      var img = new Image();
      $(img).load(function() {
      	onNUFLoadSucceed();
      });
      img.src =  'images/TXT_WantMore.jpg';
    },
  onShow: function(pageDiv) {
      $("body").addClass("transparent");
    }
}

yoinkJS.splashScreen = {
  onLoad: function(pageDiv) {
      $('.closeBtn', pageDiv).click(function() {
		    trackExitNUF();
        showExitPopup(function(confirmExit) {
            if (!!confirmExit) {
            	trackConfirmExit();
              $(document).trigger("contentsFinished", [{}]);
            }
          });
        // return to game
      });
      $('.facebookBtn', pageDiv).click(function() {
        loginType = "facebook";
        showWaitingPopup();
       	window.YGJsLib.registerUser("", "", "onFacebookLogin");
      });
      
      var img = new Image();
      $(img).load(function() {
      	onNUFLoadSucceed();
      });
      img.src =  'images/BG_Splash.jpg';
    },
  onShow: function(pageDiv) {
      $("body").removeClass("transparent");
  }
}

yoinkJS.unlockLevel = {
  onShow: function(pageDiv) {
			setTimeout(function() {
				mobileLite.navigateTo($(".redirectBtn", pageDiv), "splashScreen.html");
			}, 1);
    }
}

yoinkJS.emailSignIn01 = {
  onLoad: function(pageDiv) {
      $('.facebookBtn', pageDiv).click(function() {
        loginType = "facebook";
        showWaitingPopup();
        window.YGJsLib.registerUser("", "", "onFacebookLogin");
      });
    }
}

yoinkJS.facebookConnect = {
  onShow: function(pageDiv) {
     	onNUFLoadSucceed();
      loginType = "facebook";
      showWaitingPopup();
      window.YGJsLib.registerUser("", "", "onFacebookLogin");
    }
}

function onEmailLogin(encodedJSON)
{
  var json = decodeURIComponent2(encodedJSON);
  var data = JSON.parse(json);
  hideWaitingPopup();
  if (!!data.statusCode) {
	  if (data.statusCode > 10000) {
	    // errors
	    var errors = {};
	    if (!!data.formError) {
	      errors.email = (!!data.formError.email)?data.formError.email:[];
	      errors.password = (!!data.formError.password)?data.formError.password:[];
	    } else {
	      errors.email = [data.message];
	    }
	    updateFormValidationStatus($("#emailSignIn02"), errors);
	    $("#emailSignIn02 input[name='password']").val('');
	    $("#emailSignIn02 input[name='password']").trigger('change');
	    $("#emailSignIn02 .passwordResult").removeClass('resultGood').removeClass('resultBad');
	    
	    showErrorPopup(errors);
	    return;
  	} else if (data.statusCode == 1001) {	// disconnected	
  		showErrorPopup({'network':['Network is disconnected, please check settings.']});
	 		return;
  	} else if (data.statusCode >= 300){
  		showErrorPopup({'email':['Email login error.']});
  		return;
  	}
  }

	// No errors
	yoink_token = data.yoink_token;
	yid = data.yid;
	yuser = {yid: data.yid, yoink_token:data.yoink_token,
	         display_name: data.display_name};
	if (!!data.gender) yuser.gender = data.gender;
	if (!!data.first_name) yuser.first_name = data.first_name;
	if (!!data.last_name) yuser.last_name = data.last_name;
	if (!!data.birthday) yuser.birthday = data.birthday;

  YGJsLibAddLog(trackConfig.relogin);
	
	$(document).trigger("contentsFinished", [yuser]);
}

function onEmailRegister(encodedJSON)
{
  var json = decodeURIComponent2(encodedJSON);
  var data = JSON.parse(json);
  hideWaitingPopup();
  if (!!data.statusCode) {
	  if (data.statusCode > 10000) {
	    // errors
	    var errors = {};
	    if (!!data.formError) {
	      errors.email = (!!data.formError.email)?data.formError.email:[];
	      errors.password = (!!data.formError.password)?data.formError.password:[];
	    } else {
	      errors.email = [data.message];
	    }
	    updateFormValidationStatus($("#emailSignIn02"), errors);
	    $("#emailSignIn02 input[name='password']").val('');
	    $("#emailSignIn02 input[name='password']").trigger('change');
	    $("#emailSignIn02 .passwordResult").removeClass('resultGood').removeClass('resultBad');
	    
	    showErrorPopup(errors);
	    return;
  	} else if (data.statusCode == 1001) {	// disconnected	
  		showErrorPopup({'network':['Network is disconnected, please check settings.']});
	 		return;
  	} else if (data.statusCode >= 300){
  		showErrorPopup({'email':['Email registration error.']});
  		return;
  	}
	}	  
  YGJsLibAddLog(trackConfig.emailsignup);
	
	yoink_token = data.yoink_token;
	yid = data.yid;
	yuser = {yid: data.yid, yoink_token:data.yoink_token,
	         display_name: data.display_name};
	setTimeout(function() {
		mobileLite.navigateTo($("#emailSignIn02 .loginBtn"), "tosAgreement.html");
	}, 1);
}

yoinkJS.emailSignIn02 = {
  onLoad: function(pageDiv) {
      var errors = {email: new Array(), password: new Array(), passwordConfirm: new Array()};

      function validateForm(fields)
      {
        var success = true;
        if (!fields) {
          // default fields
          fields = {email: true, password: true};
        }
        if (!!fields.email) {
          var email = $("input[name='email']", pageDiv).val();
          errors.email = new Array();
          if (email.length == 0) {
            errors.email.push('You need to enter a email address.');
          }
          if (email.length > 64) {
            errors.email.push('Email should be no more than 64 characters.');
          }
          if (email.length > 0 && !email.match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i)) {
            errors.email.push('A valid email address is required.');
          }
        }
        if (!!fields.password) {
          var password = $("input[name='password']", pageDiv).val();
          errors.password = new Array();
          if (password.length == 0) {
            errors.password.push('You need to enter the password.');
          } else if (password.length < 6) {
            errors.password.push('Password should be 6 characters or over.');
          }
        }
        if (!!fields.passwordConfirm) {
          var passwordConfirm = $("input[name='passwordConfirm']", pageDiv).val();
          errors.passwordConfirm = new Array();
          if (password != passwordConfirm) {
            errors.passwordConfirm.push('Two passwords are not identical');
          }
        }
        success = updateFormValidationStatus(pageDiv, errors, fields, false);
        return success;
      } // end of validateForm()
      
      $("#registerPopup", pageDiv).each(function() {
        var popup = $(this);
        $(".closeBtn", this).click(function() {
          popup.hide();
        });
        $(".submitBtn", this).click(function() {
          popup.hide();
          if (validateForm(errors)) {
            loginType = "email";
            showWaitingPopup();
            window.YGJsLib.registerUser(
              $("input[name='email']", pageDiv).val(),
              $("input[name='password']", pageDiv).val(),
              "onEmailRegister"
            );
          } else {
            $("input[name='password']", pageDiv).val('');
            $("input[name='password']", pageDiv).trigger('change');
	    			$(".passwordResult", pageDiv).removeClass('resultGood').removeClass('resultBad');
            showErrorPopup(errors);
          }
        });
      });

      $("input[name='email']", pageDiv).change(function() {
        validateForm({email: true});
      });
      $("input[name='password']", pageDiv).change(function() {
        validateForm({password: true});
      });

      $(".registerBtn", pageDiv).click(function(e) {
        if (validateForm({email: true, password: true})) {
          var popup = $("#registerPopup", pageDiv);
          popup.show();
          $(".PopupresultEntry", popup).hide();
          $("input[name='passwordConfirm']", pageDiv).val('');
          $("input[name='passwordConfirm']", pageDiv).trigger('change');
        } else {
          showErrorPopup(errors);
        }
      });

      $(".loginBtn", pageDiv).click(function(e) {
        if (validateForm({email: true, password: true})) {
          loginType = "email";
          showWaitingPopup();
          window.YGJsLib.authenticateUser(
            $("input[name='email']", pageDiv).val(),
            $("input[name='password']", pageDiv).val(),
            "onEmailLogin"
          );
        } else {
          showErrorPopup(errors);
        }
      });
    }
};

yoinkJS.tosAgreement = {
  onLoad: function(pageDiv) {
      var errors = {};

      $("input[name='agree']", pageDiv).change(function() {
          if ($(this).is(':checked')) {
            $(".BGMediumToSAgreement").css('background-position', 'bottom');
          } else {
            $(".BGMediumToSAgreement").css('background-position', 'top');
          }
      });

      function validateForm(fields)
      {
        var success = true;
        if (!fields) {
          fields = {agree: true};
        }
        if (!!fields.agree) {
          if (!$("input[name='agree']", pageDiv).is(':checked')) {
            errors.agree = ['You need to check the checkbox to proceed.'];
            success = false;
          }
        }
        return success;
      }
     
      $(".btnAgree", pageDiv).click(function() {
        if (validateForm()) {
					setTimeout(function() {
			      mobileLite.navigateTo($(this), "nickname.html");
					}, 1);
        } else {
          showErrorPopup(errors);
        }
      });
      
    }
};

yoinkJS.PP = {
  onShow: function(pageDiv) {
      YGJsLibAddLog(trackConfig.ppview);
    }
};

yoinkJS.ToS = {
  onShow: function(pageDiv) {
      YGJsLibAddLog(trackConfig.tosview);
    }
};

function onSaveNickname(encodedJSON)
{
  var data = JSON.parse(decodeURIComponent2(encodedJSON));
  
  hideWaitingPopup();

	if (!!data.statusCode) {  
	  if (data.statusCode > 10000) {
	    // errors
	    var errors = {};
	    if (!!data.formError) {
	      errors.nickname = (!!data.formError.display_name)?data.formError.display_name:[];
	    } else {
	      errors.nickname = [data.message];
	    }
	    updateFormValidationStatus($("#nickname"), errors, null, true);
	
	    showErrorPopup(errors);
      $(".BGMediumNickname").addClass("BGAvatarBad").removeClass("BGAvatarGood");
	    return;
  	} else if (data.statusCode == 1001) {	// disconnected	
  		showErrorPopup({'network':['Network is disconnected, please check settings.']});
	 		return;
  	} else if (data.statusCode >= 300){
  		showErrorPopup({'nickname':['Saving nickname error.']});
      $(".BGMediumNickname").addClass("BGAvatarBad").removeClass("BGAvatarGood");
  		return;
  	}
	}
	
  $(".BGMediumNickname").addClass("BGAvatarGood").removeClass("BGAvatarBad");
	
	yuser.display_name = $("#nickname input[name='nickname']").val();
	if (loginType == "facebook") {
		setTimeout(function() {
		  mobileLite.navigateTo($("#nickname .submitBtn"), "thankyouScreen.html");
		}, 1);
	} else {
		setTimeout(function() {
		  mobileLite.navigateTo($("#nickname .submitBtn"), "realname.html");
		}, 1);
	}
}

yoinkJS.nickname = {
  onLoad: function(pageDiv) {
      var errors = {nickname: new Array()};

      function validateForm(fields)
      {
        var success = true;
        if (!fields) {
          // default fields
          fields = {nickname: true};
        }
        if (!!fields.nickname) {
          var nickname = $("input[name='nickname']", pageDiv).val();
          errors.nickname = new Array();
          if (nickname.length < 4 || nickname > 16) {
            errors.nickname.push('Nickname must be 4 - 16 characters.');
          } else if (!nickname.match(/^[a-z0-9\-_]+$/i)) {
            errors.nickname.push('Nickname should only have numbers, letters, _ and -.');
          }
          if (errors.nickname.length > 0) {
            $(".BGMediumNickname").addClass("BGAvatarBad").removeClass("BGAvatarGood");
          } else {
            $(".BGMediumNickname").addClass("BGAvatarGood").removeClass("BGAvatarBad");
          }
        }
        success = updateFormValidationStatus(pageDiv, errors, fields, true, true);
        return success;
      } // end of validateForm()
      
      $("input[name='nickname']", pageDiv).change(function() {
        validateForm({nickname: true});
      });

      $(".submitBtn", pageDiv).click(function(e) {
        if (validateForm()) {
          showWaitingPopup();
          window.YGJsLib.setUserProfile(JSON.stringify({
                yoink_token: yoink_token,
                display_name: $("input[name='nickname']").val()
              }), "onSaveNickname");
        } else {
          showErrorPopup(errors);
        }
      });
    }
};

function onSaveRealname(encodedJSON)
{
  var data = JSON.parse(decodeURIComponent2(encodedJSON));
  
  hideWaitingPopup();

	if (!!data.statusCode) {
	  if (data.statusCode > 10000) {
	    // errors
	    var errors = {};
	    if (!!data.formError) {
	      errors.first_name = (!!data.formError.first_name)?data.formError.first_name:[];
	      errors.last_name = (!!data.formError.last_name)?data.formError.last_name:[];
	    } else {
	      errors.nickname = [data.message];
	    }
	    updateFormValidationStatus($("#realname"), errors);
	    
	    showErrorPopup(errors);
	    return;
  	} else if (data.statusCode == 1001) {	// disconnected	
  		showErrorPopup({'network':['Network is disconnected, please check settings.']});
	 		return;
  	} else if (data.statusCode >= 300){
  		showErrorPopup({'realname':['Saving real name error.']});
  		return;
  	}
  }
  
	yuser.first_name = $("#realname input[name='first_name']").val();
	yuser.last_name = $("#realname input[name='last_name']").val();
	setTimeout(function() {
		mobileLite.navigateTo($("#realname .submitBtn"), "userInfo.html");
	}, 1);
}

yoinkJS.realname = {
  onLoad: function(pageDiv) {
      var errors = {first_name: new Array(), last_name: new Array()};

      function validateForm(fields)
      {
        var success = true;
        if (!fields) {
          // default fields
          fields = {first_name: true, last_name: true};
        }
        if (!!fields.first_name) {
          var first_name = $("input[name='first_name']", pageDiv).val();
          errors.first_name = new Array();
          if (first_name.length == 0) {
            errors.first_name.push('Please enter your first name.');
          } else {
            if (first_name.length > 32) {
              errors.first_name.push('First name must be 32 or less characters.');
            }
            if (!first_name.match(/^[a-z0-9 ]+$/i)) {
              errors.first_name.push('First name should only have numbers, letters, and spaces');
            }
          }
        }
        if (!!fields.last_name) {
          var last_name = $("input[name='last_name']", pageDiv).val();
          errors.last_name = new Array();
          if (last_name.length == 0) {
            errors.last_name.push('Please enter your last name.');
          } else {
            if (last_name.length > 32) {
              errors.last_name.push('Last name must be 32 or less characters.');
            }
            if (!last_name.match(/^[a-z0-9 ]+$/i)) {
              errors.last_name.push('Last name should only have numbers, letters, and spaces');
            }
          }
        }

        success = updateFormValidationStatus(pageDiv, errors, fields);
        return success;
      } // end of validateForm()
     
      $("input[name='last_name']", pageDiv).change(function() {
        validateForm({last_name: true});
      });
      $("input[name='first_name']", pageDiv).change(function() {
        validateForm({first_name: true});
      });

			$(".skipBtn", pageDiv).click(function() {
				trackSkipNUF($(pageDiv).attr("id"));
			});     

      $(".submitBtn", pageDiv).click(function(e) {
        if (validateForm()) {
          showWaitingPopup();
          window.YGJsLib.setUserProfile(JSON.stringify({
                yoink_token: yoink_token,
                first_name: $("input[name='first_name']", pageDiv).val(),
                last_name: $("input[name='last_name']", pageDiv).val(),
              }), "onSaveRealname");
        } else {
          showErrorPopup(errors);
        }
      });
    }
};

function onSaveNicknameRealname(encodedJSON)
{
  var data = JSON.parse(decodeURIComponent2(encodedJSON));
  
  hideWaitingPopup();

	if (!!data.statusCode) {	
	  if (data.statusCode > 10000) {
	    // errors
	    var errors = {};
	    if (!!data.formError) {
	      errors.nickname = (!!data.formError.display_name)?data.formError.display_name:[];
	      errors.first_name = (!!data.formError.first_name)?data.formError.first_name:[];
	      errors.last_name = (!!data.formError.last_name)?data.formError.last_name:[];
	    } else {
	      errors.nickname = [data.message];
	    }
	    updateFormValidationStatus($("#nicknameRealname"), errors);
	    
	    showErrorPopup(errors);
	    return;
  	} else if (data.statusCode == 1001) {	// disconnected	
  		showErrorPopup({'network':['Network is disconnected, please check settings.']});
	 		return;
  	} else if (data.statusCode >= 300){
  		showErrorPopup({'nicknameRealname':['Saving nickname and real name error.']});
  		return;
  	}
	}    

	yuser.display_name = $("#nicknameRealname input[name='nickname']").val();
	yuser.first_name = $("#nicknameRealname input[name='first_name']").val();
	yuser.last_name = $("#nicknameRealname input[name='last_name']").val();
	setTimeout(function() {
		mobileLite.navigateTo($("#nicknameRealname .submitBtn"), "userInfo.html");
	}, 1);
}

yoinkJS.nicknameRealname = {
  onLoad: function(pageDiv) {
      var errors = {nickname: new Array(), first_name: new Array(), last_name: new Array()};

      function validateForm(fields)
      {
        var success = true;
        if (!fields) {
          // default fields
          fields = {nickname: true, first_name: true, last_name: true};
        }
        if (!!fields.nickname) {
          var nickname = $("input[name='nickname']", pageDiv).val();
          errors.nickname = new Array();
          if (nickname.length < 4 || nickname > 16) {
            errors.nickname.push('Nickname must be 4 - 16 characters.');
          } else if (!nickname.match(/^[a-z0-9\-_]+$/i)) {
            errors.nickname.push('Nickname should only have numbers, letters, _ and -.');
          }
        }
        if (!!fields.first_name) {
          var first_name = $("input[name='first_name']", pageDiv).val();
          errors.first_name = new Array();
          if (first_name.length == 0) {
            errors.first_name.push('Please enter your first name.');
          } else {
            if (first_name.length > 32) {
              errors.first_name.push('First name must be 32 or less characters.');
            }
            if (!first_name.match(/^[a-z0-9 ]+$/i)) {
              errors.first_name.push('First name should only have numbers, letters, and spaces');
            }
          }
        }
        if (!!fields.last_name) {
          var last_name = $("input[name='last_name']", pageDiv).val();
          errors.last_name = new Array();
          if (last_name.length == 0) {
            errors.last_name.push('Please enter your last name.');
          } else {
            if (last_name.length > 32) {
              errors.last_name.push('Last name must be 32 or less characters.');
            }
            if (!last_name.match(/^[a-z0-9 ]+$/i)) {
              errors.last_name.push('Last name should only have numbers, letters, and spaces');
            }
          }
        }

        success = updateFormValidationStatus(pageDiv, errors, fields);
        return success;
      } // end of validateForm()
     
      $("input[name='nickname']", pageDiv).change(function() {
        validateForm({nickname: true});
      });
      $("input[name='last_name']", pageDiv).change(function() {
        validateForm({last_name: true});
      });
      $("input[name='first_name']", pageDiv).change(function() {
        validateForm({first_name: true});
      });

      $(".submitBtn", pageDiv).click(function(e) {
        if (validateForm()) {
          showWaitingPopup();
          window.YGJsLib.setUserProfile(JSON.stringify({
                yoink_token: yoink_token,
                display_name: $("input[name='nickname']", pageDiv).val(),
                first_name: $("input[name='first_name']", pageDiv).val(),
                last_name: $("input[name='last_name']", pageDiv).val(),
              }), "onSaveNicknameRealname");
        } else {
          showErrorPopup(errors);
        }
      });
    }
};

function onSaveUserInfo(encodedJSON)
{
  var data = JSON.parse(decodeURIComponent2(encodedJSON));

  hideWaitingPopup();
  
  if (!!data.statusCode) {
	  if (data.statusCode > 10000) {
	    // errors
	    var errors = {};
	    if (!!data.formError) {
	      errors.gender = (!!data.formError.gender)?data.formError.gender:[];
	      errors.birthday = (!!data.formError.birthday)?data.formError.birthday:[];
	    } else {
	      errors.gender = [data.message];
	    }
	    
	    showErrorPopup(errors);
	    return;
  	} else if (data.statusCode == 1001) {	// disconnected	
  		showErrorPopup({'network':['Network is disconnected, please check settings.']});
	 		return;
  	} else if (data.statusCode >= 300){
  		showErrorPopup({'userInfo':['Saving user info error.']});
  		return;
  	}	  
  }
   
	$("#userInfo .datePickerWidget").trigger('getDate', 
	  [function(month, day, year) {
	      yuser.gender = $("#userInfo input[name='gender']:checked").val()=='male'?1:2;
	      yuser.birthday = month+'/'+day+'/'+year;
				setTimeout(function() {
	      	mobileLite.navigateTo($("#userInfo .submitBtn"), "thankyouScreen.html");
				}, 1);
	  }]);
}

yoinkJS.userInfo = {
  onLoad: function(pageDiv) {
      var errors = {};

      function validateForm(fields)
      {
        var success = true;
        if (!fields) {
          fields = {gender: true};
        }
        if (!!fields.gender) {
          if ($("input[name='gender']:checked", pageDiv).length == 0) {
            errors.agree = ['You need to select a gender.'];
            success = false;
          }
        }
        return success;
      }

			$(".skipBtn", pageDiv).click(function() {
				trackSkipNUF($(pageDiv).attr("id"));
			});     
      $(".submitBtn", pageDiv).click(function() {
        if (validateForm()) {
          showWaitingPopup();
          $(".datePickerWidget", pageDiv).trigger('getDate', 
            [function(month, day, year) {
              window.YGJsLib.setUserProfile(JSON.stringify({
                    yoink_token: yoink_token,
                    gender: $("input[name='gender']:checked", pageDiv).val()=='male'?1:2,
                    birthday: month+'/'+day+'/'+year
                  }), "onSaveUserInfo");
            }]);
        } else {
          showErrorPopup(errors);
        }
      });
    }
};

yoinkJS.thankyouScreen = {
  onLoad: function(pageDiv) {
      $(".returnGameBtn", pageDiv).click(function() {
        yuser.yid = yid;
        yuser.yoink_token = yoink_token;
        $(document).trigger("contentsFinished", [yuser]);
        // return to game
      });
    }
};
