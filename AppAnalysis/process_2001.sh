#!/bin/bash
./gradlew build
apk_set=/home/chenjun/benign/benign_application_2001/*
idx=0
for apk in ${apk_set}
do
	echo "$apk"
	echo "the number of apk:2001"
	let "idx += 1"
	echo ""$idx"th Apk: ${apk##*/} "
	timeout 1200 ./IntelliDroidAppAnalysis -o /home/chenjun/benign/benign_application_2001_result/${apk##*/} $apk
	mv $apk /home/chenjun/benign/benign_application_2001_done
	echo 'apk have been moved'
done
