#!/bin/bash
./gradlew build
apk_set=/home/chenjun/benign/benign_application/*
idx=0

for apk in ${apk_set}
do
	echo "$apk"
	let "idx += 1"
	echo ""$idx"th Apk: ${apk##*/} "
	timeout 1800 ./IntelliDroidAppAnalysis -o /home/chenjun/benign_result/${apk##*/} $apk
	mv $apk /home/chenjun/benign_done
	echo 'move done'
done

