#!/bin/bash
./gradlew build
apk_set=/home/chenjun/benign/benign_application_2002/*
idx=8
for apk in ${apk_set}
do
	echo "$apk"
	echo "the number of apk:2002"
	let "idx += 1"
	echo ""$idx"th Apk: ${apk##*/} "
	timeout 1200 ./IntelliDroidAppAnalysis -o /home/chenjun/benign/benign_application_2002_result/${apk##*/} $apk
	mv $apk /home/chenjun/benign/benign_application_2002_done
	echo 'apk have been moved'
done
