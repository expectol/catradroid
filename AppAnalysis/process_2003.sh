#!/bin/bash
./gradlew build
apk_set=/home/chenjun/benign/benign_application_2003/*
idx=56
for apk in ${apk_set}
do
	echo "$apk"
	echo "the number of apk:2003"
	let "idx += 1"
	echo ""$idx"th Apk: ${apk##*/} "
	timeout 1200 ./IntelliDroidAppAnalysis -o /home/chenjun/benign/benign_application_2003_result/${apk##*/} $apk
	mv $apk /home/chenjun/benign/benign_application_2003_done
	echo 'apk have been moved'
done
